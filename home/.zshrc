zmodload zsh/zprof

# Gaijin no touch me file{{{
# Sudo has to be modified so that it usages the 022 mask
# https://superuser.com/questions/79914/how-do-i-tell-sudo-to-write-files-with-a-umask-of-0022
umask 027 
sudo() {
    old=$(umask)
    umask 022
    command sudo "$@"
    umask $old
}
# }}}

# Shell propmt history {{{
# ====================
export HISTFILE=$HOME/.cache/history
export SAVEHIST=1000
export HISTSIZE=1000
# }}}

# Custom prompt{{{
# =============
# It will look like this: `user@host:somedir $ ls -alh`
export PS1="%B%F{green}%n@%m%f:%F{blue}%1c%f$%b "
# }}}

# Remove / from list of character recognized as word{{{
export WORDCHARS=${WORDCHARS/\/}
# }}}

# KEYBINDING {{{
# https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/lib/key-bindings.zsh

if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
  function zle-line-init() {
    echoti smkx
  }
  function zle-line-finish() {
    echoti rmkx
  }
  zle -N zle-line-init
  zle -N zle-line-finish
fi

# Use vim key bindings
bindkey -v

# [PageUp] - Up a line of history
if [[ -n "${terminfo[kpp]}" ]]; then
  bindkey -M emacs "${terminfo[kpp]}" up-line-or-history
  bindkey -M viins "${terminfo[kpp]}" up-line-or-history
  bindkey -M vicmd "${terminfo[kpp]}" up-line-or-history
fi
# [PageDown] - Down a line of history
if [[ -n "${terminfo[knp]}" ]]; then
  bindkey -M emacs "${terminfo[knp]}" down-line-or-history
  bindkey -M viins "${terminfo[knp]}" down-line-or-history
  bindkey -M vicmd "${terminfo[knp]}" down-line-or-history
fi

# Start typing + [Up-Arrow] - fuzzy find history forward
if [[ -n "${terminfo[kcuu1]}" ]]; then
  autoload -U up-line-or-beginning-search
  zle -N up-line-or-beginning-search

  bindkey -M emacs "${terminfo[kcuu1]}" up-line-or-beginning-search
  bindkey -M viins "${terminfo[kcuu1]}" up-line-or-beginning-search
  bindkey -M vicmd "${terminfo[kcuu1]}" up-line-or-beginning-search
fi
# Start typing + [Down-Arrow] - fuzzy find history backward
if [[ -n "${terminfo[kcud1]}" ]]; then
  autoload -U down-line-or-beginning-search
  zle -N down-line-or-beginning-search

  bindkey -M emacs "${terminfo[kcud1]}" down-line-or-beginning-search
  bindkey -M viins "${terminfo[kcud1]}" down-line-or-beginning-search
  bindkey -M vicmd "${terminfo[kcud1]}" down-line-or-beginning-search
fi

# [Home] - Go to beginning of line
if [[ -n "${terminfo[khome]}" ]]; then
  bindkey -M emacs "${terminfo[khome]}" beginning-of-line
  bindkey -M viins "${terminfo[khome]}" beginning-of-line
  bindkey -M vicmd "${terminfo[khome]}" beginning-of-line
fi
# [End] - Go to end of line
if [[ -n "${terminfo[kend]}" ]]; then
  bindkey -M emacs "${terminfo[kend]}"  end-of-line
  bindkey -M viins "${terminfo[kend]}"  end-of-line
  bindkey -M vicmd "${terminfo[kend]}"  end-of-line
fi

# [Shift-Tab] - move through the completion menu backwards
if [[ -n "${terminfo[kcbt]}" ]]; then
  bindkey -M emacs "${terminfo[kcbt]}" reverse-menu-complete
  bindkey -M viins "${terminfo[kcbt]}" reverse-menu-complete
  bindkey -M vicmd "${terminfo[kcbt]}" reverse-menu-complete
fi

# [Backspace] - delete backward
bindkey -M emacs '^?' backward-delete-char
bindkey -M viins '^?' backward-delete-char
bindkey -M vicmd '^?' backward-delete-char
# [Delete] - delete forward
if [[ -n "${terminfo[kdch1]}" ]]; then
  bindkey -M emacs "${terminfo[kdch1]}" delete-char
  bindkey -M viins "${terminfo[kdch1]}" delete-char
  bindkey -M vicmd "${terminfo[kdch1]}" delete-char
else
  bindkey -M emacs "^[[3~" delete-char
  bindkey -M viins "^[[3~" delete-char
  bindkey -M vicmd "^[[3~" delete-char

  bindkey -M emacs "^[3;5~" delete-char
  bindkey -M viins "^[3;5~" delete-char
  bindkey -M vicmd "^[3;5~" delete-char
fi

# [Ctrl-Delete] - delete whole forward-word
bindkey -M emacs '^[[3;5~' kill-word
bindkey -M viins '^[[3;5~' kill-word
bindkey -M vicmd '^[[3;5~' kill-word

# [Ctrl-RightArrow] - move forward one word
bindkey -M emacs '^[[1;5C' forward-word
bindkey -M viins '^[[1;5C' forward-word
bindkey -M vicmd '^[[1;5C' forward-word
# [Ctrl-LeftArrow] - move backward one word
bindkey -M emacs '^[[1;5D' backward-word
bindkey -M viins '^[[1;5D' backward-word
bindkey -M vicmd '^[[1;5D' backward-word


bindkey '\ew' kill-region                             # [Esc-w] - Kill from the cursor to the mark
bindkey -s '\el' 'ls\n'                               # [Esc-l] - run command: ls
bindkey '^r' history-incremental-search-backward      # [Ctrl-r] - Search backward incrementally for a specified string. The string may begin with ^ to anchor the search to the beginning of the line.
bindkey ' ' magic-space                               # [Space] - don't do history expansion

# Edit the current command line in $EDITOR
autoload -U edit-command-line
zle -N edit-command-line
bindkey '\C-x\C-e' edit-command-line

# file rename magick
bindkey "^[m" copy-prev-shell-word

# FZF
if [ -e /usr/share/doc/fzf/key-bindings.zsh ]; then
	source /usr/share/doc/fzf/key-bindings.zsh
	bindkey "^p" fzf-file-widget
	# bindkey "^P" fzf-cd-widget
	bindkey "^r" fzf-history-widget
fi
# }}}

# FUZZY MATCHING {{{
autoload -Uz compinit
compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list '' \
  'm:{a-z\-}={A-Z\_}' \
  'r:[^[:alpha:]]||[[:alpha:]]=** r:|=* m:{a-z\-}={A-Z\_}' \
  'r:|?=** m:{a-z\-}={A-Z\_}'
# }}}

# REMOVE THE % EOL{{{
unsetopt prompt_cr prompt_sp
# }}}

# Custom functions{{{
# ================
sessionctl () { systemctl --user $@ } # shortcut for systemctl --user

mkemptyfile () {
    dd if=/dev/zero of=$2 bs=1M count=$1
}

# src () { exec zsh } # Restart shell inplace

ssh-init () { # Initialize ssh agent
	eval $(ssh-agent -s | grep -v echo)
	for f in $HOME/.ssh/*id_rsa; do
		ssh-add $f
	done
}
# }}}

# Source whatever files in ~/.zshd if available{{{
# =============================================
if [ -d ~/.zshd ]; then
  for f in $(ls ~/.zshd); do
    source ~/.zshd/$f
  done
fi
fpath+=~/.zfunc
# }}}

# Ignore some commands pattern from history{{{
# Some of these are not very useful when remembered
HISTORY_IGNORE="systemctl*poweroff*|systemctl*reboot" # Fuck these
HISTORY_IGNORE="$HISTORY_IGNORE|fortune"
HISTORY_IGNORE="$HISTORY_IGNORE|cd .."
HISTORY_IGNORE="$HISTORY_IGNORE|cd ."
HISTORY_IGNORE="$HISTORY_IGNORE|ls|l|ll"
HISTORY_IGNORE="$HISTORY_IGNORE|src"
HISTORY_IGNORE="$HISTORY_IGNORE|make"
HISTORY_IGNORE="$HISTORY_IGNORE|Thunar ."
HISTORY_IGNORE="$HISTORY_IGNORE|Thunar"
HISTORY_IGNORE="$HISTORY_IGNORE|dragon"
HISTORY_IGNORE="$HISTORY_IGNORE|vlc"
HISTORY_IGNORE="$HISTORY_IGNORE|nvim"
HISTORY_IGNORE="$HISTORY_IGNORE|nvim*HISTFILE"
HISTORY_IGNORE="$HISTORY_IGNORE|julia|python"
# }}}

# Nix {{{
if [ -e '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh' ]; then
  . '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh'
fi
if [ -e $HOME/.nix-profile/etc/profile.d/nix.sh ]; then
  . $HOME/.nix-profile/etc/profile.d/nix.sh
fi
# }}}

# Restart zsh
src () { exec zsh }

# vim: foldmethod=marker foldlevel=0
