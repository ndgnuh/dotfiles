# Nix
if [ -e '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh' ]; then
  . '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh'
fi
if [ -e $HOME/.nix-profile/etc/profile.d/nix.sh ]; then
  . $HOME/.nix-profile/etc/profile.d/nix.sh
fi

# This file contains
# - environment variables
# - aliases

# Helpers 
# =======
trysource() { [ -f "$1" ] && source "$1"; }
trypath() { [ -d "$1" ] && export PATH="$1:$PATH"; };

# Standard variables
# ==================
export EDITOR=nvim
if command -v firefox-devedition > /dev/null; then
    export BROWSER=firefox-devedition
else
    export BROWSER=firefox
fi
export CASE_SENSITIVE=true

# External paths and environments
# ===============================
trypath "$HOME/.nimble/bin"
trypath "$HOME/.local/bin"
trypath "$HOME/.yarn/bin"
trypath "$HOME/.luarocks/bin"
trysource "$HOME/.nix-profile/etc/profile.d/nix.sh"
trysource "$HOME/.cargo/env"
trysource "$HOME/.lazy_node_loader.sh"
trysource "$HOME/.fzf.sh"

# Julialang
# =========
export JULIA_NUM_THREADS=8
export JULIA_DEPOT_PATH="$HOME/.cache/julia"
trypath "$HOME/.cache/julia/juliaup/bin" # juliaup, do not add when reinstall
trypath "$HOME/.cache/julia/bin"

# Android and Flutter
# ===================
export ANDROID_HOME=$HOME/Application/Android/
export ANDROID_SDK_ROOT=$ANDROID_HOME
export CHROME_EXECUTABLE="chromium"
trypath "$FLUTTER_HOME/bin"
trypath "$ANDROID_HOME/cmdline-tools/latest/bin" 
trypath "$ANDROID_HOME/platform-tools"
trypath "$ANDROID_HOME/tools"
trypath "$ANDROID_HOME/tools/bin"
trypath "$ANDROID_HOME/emulator/bin64"
trypath "$ANDROID_HOME/emulator"

# Setup IBus
# ==========
# Some application does not work with global config, thus the alias.
export GTK_IM_MODULE=ibus
export QT_IM_MODULE=ibus
export QT4_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
alias teams="GTK_IM_MODULE=xim teams"
alias chromium="GTK_IM_MODULE=xim chromium"

export QT_QPA_PLATFORMTHEME="qt5ct"
export XCURSOR_SIZE=48

# Libreoffice use QT theme
# ========================
export SAL_USE_VCLPLUGIN=kf5

# Helpful aliases
# ===============
alias ls="ls -h --color=auto"
alias l="ls -lh --color=auto"
alias feh="feh --scale-down"
alias gc='git commit -v'
alias gcm='git commit -v -m'
alias ga='git add'
alias gpsh='git push'
alias gpll='git pull'
alias gco='git pull'
