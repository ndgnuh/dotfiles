# Dot-files

This repository contains dot-files, which is managed using GNU Stow.

To install, run `stow -t ~/ <app-name>`, where app-name is replaced with a directory name, such as `sakura`, `nvim`, etc.
