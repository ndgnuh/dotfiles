#!/bin/sh
TARGETDIR="$HOME"
for d in home bamboo nvim sakura; do
	if [ -d $d ]; then
		stow -t "${TARGETDIR}" "${d}"
	fi
done
