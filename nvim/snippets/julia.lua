require("luasnip.session")

local sym = function(trig, a)
    return s(trig, t(a))
end

return {
    -- function definitions
    s("fun", {
        t("function "),
        i(1, "f"),
        t("("),
        i(2),
        t(")"),
        i(3, "return"),
    }),

    -- branching
    s("if", {
        t { "if " },
        i(1, "true"),
        t { " end" }
    }),

    s("?:", {
        i(1, "condition"),
        t { " ? " },
        i(2, "true"),
        t { " : " },
        i(3, "false"),
    }),

    -- loops
    s("for", {
        t { "for " },
        i(1, "i"),
        t { " in " },
        i(2, "collection"),
        t { "", "\t" },
        i(3, "# do something"),
        t { "", "end" },
    }),

    s("while", {
        t { "while " },
        i(1, "condition"),
        t { "", "\t" },
        i(2, "# do something"),
        t { "", "end" },
    }),

    -- macros
    s("info", {
        t { "@info " },
        i(1, "true"),
    }),

    s("warn", {
        t { "@warn " },
        i(1, "true"),
    }),

    s("error", {
        t { "@error " },
        i(1, "true"),
    }),

    -- other structures
    s("let", { t { "let", "\t" }, i(1, "# content"), t { "", "end" } }),
    s("beg", { t { "begin", "\t" }, i(1, "# content"), t { "", "end" } }),
    s("try", {
        t { "try", "\t" },
        i(1, "# content"),
        t { "", "catch " },
        i(2, "exc"),
        t { "", "\t" },
        i(3, "# handle error"),
        t { "", "end" },
    }),

    -- greeks
    sym("alpha", "α"),
}
