-- Abbreviations used in this article and the LuaSnip docs
local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
local isn = ls.indent_snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node
local events = require("luasnip.util.events")
local ai = require("luasnip.nodes.absolute_indexer")
local extras = require("luasnip.extras")
local l = extras.lambda
local rep = extras.rep
local p = extras.partial
local m = extras.match
local n = extras.nonempty
local dl = extras.dynamic_lambda
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local conds = require("luasnip.extras.expand_conditions")
local postfix = require("luasnip.extras.postfix").postfix
local types = require("luasnip.util.types")
local parse = require("luasnip.util.parser").parse_snippet
local ms = ls.multi_snippet
local k = require("luasnip.nodes.key_indexer").new_key

return {
    -- begin env
    s("\\begin", {
        t { "\\begin{" },
        i(1),
        t { "}", "" },
        isn(2, { i(1) }, ""),
        t { "", "\\end{" },
        f(function(args) return args[1] end, ai(1)),
        t("}%"),
    }),

    -- itemize
    s({ trig = "\\itemize" }, {
        t { "\\begin{itemize}", "" },
        isn(1, { i(1) }, "$PARENT_INDENT//"),
        t { "", "\\end{itemize}" },
    }),

    -- figure
    s("\\figure", {
        t {
            "\\begin{figure}",
            "\\centering",
            "\\includegraphics[width=\\textwidth]{"
        },
        i(1, "example-image-golden"),
        t {
            "}",
            "\\caption{",
        },
        i(2, "My figure"),
        t { "\\label{fig:" },
        i(3, "my-figure"),
        t {
            "}",
            "\\end{figure}"

        }
    }),

    -- tabular
    s({ trig = "\\tab" }, {
        t { "\\begin{tabular}{" },
        i(1, "column spec"),
        t { "}", "" },
        i(2),
        t { "", "\\end{tabular}" },
    }),

    -- usual commands
    s("\\doc", { t "\\documentclass[", i(1), t "]{", i(2), t "}" }),
    s("\\use[", { t "\\usepackage[", i(1), t "]{", i(2), t "}" }),
    s("\\use", { t "\\usepackage{", i(1), t "}" }),
    s("\\inp", { t "\\input{", i(1), t "}" }),
}
