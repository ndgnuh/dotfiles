local ls = require("luasnip")
local s = ls.snippet
require("luasnip.session")

return {
    -- imports
    s("from%s*", {
        t("from "),
        i(1, "package"),
        t(" import "),
        i(2, "name")
    }),

    s("imp", {
        t("import "),
        i(1, "package"),
    }, {}),

    s("impa", {
        t("import "),
        i(1, "package"),
        t(" as "),
        i(2, "alias")
    }, {}),

    -- definitions
    s("def", {
        t("def "),
        i(1, "hello"),
        t("("),
        i(2, "*args, **kwargs"),
        t { "):", "\t" },
        i(3, "return world"),
    }),

    s("class", {
        t("class "),
        i(1, "ClassName"),
        t("("),
        i(2, "object"),
        t { "):", "\t" },
        i(3, "pass"),
    }),

    -- branches
    s("if", {
        t { "if " },
        i(1, "condition"),
        t { ":", "\t" },
        i(2, "pass"),
    }),

    s("ife", {
        t { "if " },
        i(1, "condition"),
        t { ":", "\t" },
        i(2, "pass"),
        t { "", "else:", "\t" },
        i(3, "pass"),
    }),

    -- loops
    s("for", {
        t { "for " },
        i(1, "condition"),
        t { ":", "\t" },
        i(2, "break"),
    }),

    s("while", {
        t { "while " },
        i(1, "condition"),
        t { ":", "\t" },
        i(2, "break"),
    }),

    -- exceptions
    s("try", {
        t { "try:", "\t" },
        i(1, "pass"),
        t { "", "except Exception as e:", "\t" },
        i(2, "pass"),
        t { "", "finally:", "\t" },
        i(3, "pass"),
    }),

}
