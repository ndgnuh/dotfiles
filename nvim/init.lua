local ok, err = pcall(require, "ndgnuh.packages")
if not ok then
    vim.print("failed to load packages, error is\n" .. err)
end
require("libs.hardwrap")
require("ndgnuh.commands")
require("ndgnuh.options")
require("ndgnuh.remaps")
require("ndgnuh.autocmds")
require("statusline")
require("ibus").setup()
