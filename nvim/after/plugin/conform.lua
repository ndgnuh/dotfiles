local conform = require("conform")
conform.setup({
    formatters_by_ft = {
        -- LSP is usable anyway
        -- Conform will run multiple formatters sequentially
        -- Use a sub-list to run only the first available formatter
        -- lua = { "stylua" },
        -- python = { "isort", "black" },
        -- latex = { "latexindent" },
        -- javascript = { { "prettierd", "prettier" } },
        -- json = { "jq" },
        -- shell = { "shfmt" },
    },
    format_on_save = {
        -- These options will be passed to conform.format()
        timeout_ms = 500,
        lsp_fallback = true,
    },
})

vim.keymap.set({ "n", "x" }, "<leader>f", function()
    conform.format({ async = true, lsp_fallback = true })
end, {})
