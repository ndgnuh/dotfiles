local builtin = require('telescope.builtin')
vim.keymap.set('n', '<C-p>', builtin.find_files, {})
vim.keymap.set('n', '<leader>p', builtin.git_files, {})
vim.keymap.set('n', '<leader>g', builtin.live_grep, {})

--- Close the telescope menu with <C-c>
vim.api.nvim_create_autocmd({ "InsertLeave", "FileType" }, {
    pattern = { "TelescopePrompt" },
    group = vim.api.nvim_create_augroup("TelescopeCloseShortcut", { clear = true }),
    callback = function(ev)
        vim.keymap.set({ "x", "i", "n" }, "<C-c>", function()
            vim.api.nvim_buf_delete(ev.buf, { force = true })
        end, { buffer = ev.buf })
    end,
})
