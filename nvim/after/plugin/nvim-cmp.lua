local cmp = require('cmp')

-- setup completion and snippet
-- ============================
cmp.setup({
    -- ordered by priority
    sources = cmp.config.sources({
        {
            name = "nvim_lsp",
            keyword_length = 1,
        },
        { name = "nvim_lsp_signature_help" },
        {
            name = 'luasnip',
            max_item_count = 10,
            option = { show_autosnippets = true },
        },
        { name = "path" },
        { name = "nvim_lua" }
    }, { { name = 'buffer' } }),
    mapping = cmp.mapping.preset.insert({
        -- Enter key confirms completion item
        ['<CR>'] = cmp.mapping.confirm({ select = false }),

        -- Ctrl + space triggers completion menu
        ['<C-space>'] = cmp.mapping.complete(),
    }),
    snippet = {
        expand = function(args)
            require('luasnip').lsp_expand(args.body)
        end,
    },
})
