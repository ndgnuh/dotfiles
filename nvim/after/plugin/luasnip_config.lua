local ls = require("luasnip")
local ls_load = require("luasnip.loaders.from_lua").load
local config_path = tostring(vim.fn.stdpath("config"))
local snippets_path = vim.fs.joinpath(config_path, "snippets")

ls.config.set_config({ enable_autosnippets = true, })
ls_load({ paths = snippets_path })

vim.api.nvim_create_user_command("LuaSnipLoadL2U", function(args)
    local l2u = require("latex2unicode")
    ls.add_snippets("all", l2u)
end, {})

-- Tab conflict with julia tab behaviour, so <C-l> is backup
vim.keymap.set({ "i", "s" }, "<C-l>", ls.expand_or_jump, { silent = true })
vim.keymap.set({ "i", "s" }, "<Tab>", function()
    if ls.expand_or_jumpable() then
        ls.expand_or_jump()
    else
        local key = vim.api.nvim_replace_termcodes("<Tab>", true, false, true)
        vim.api.nvim_feedkeys(key, "n", false)
    end
end, { silent = true })

vim.keymap.set({ "i", "s" }, "<s-tab>", function()
    ls.jump(-1)
end, { silent = true })
