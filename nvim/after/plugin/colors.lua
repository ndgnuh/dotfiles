local setups = {}

-- config gruvbox
setups.gruvbox = function()
    vim.g.gruvbox_bold = true
    vim.g.gruvbox_italic = true
    vim.g.gruvbox_underline = false
    vim.g.gruvbox_undercurl = false
    vim.g.gruvbox_transparent_background = true
    vim.cmd.colorscheme("gruvbox")
end

setups.oxocarbon = function()
    vim.opt.background = "dark"
    vim.cmd.colorscheme("oxocarbon")
end

--- If no display, load the default
if os.getenv("DISPLAY") ~= "" then
    vim.cmd.colorscheme("no-clown-fiesta")
else
    vim.opt.termguicolors = false
end
