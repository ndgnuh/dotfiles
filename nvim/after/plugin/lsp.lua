local lspconfig = require('lspconfig')

local lsp_capabilities = require('cmp_nvim_lsp').default_capabilities()

local tools = require("libs")

-- lsp related hooks
local on_attach = function() end
-- require("lsp-format").setup {}
-- local on_attach = function(...)
--     require("lsp-format").on_attach(...)
-- end

-- auto installation of lsp servers,
-- disabled because of python env problems
-- require('mason').setup()
-- require('mason-lspconfig').setup()

-- configure lsp-servers
lspconfig.pylsp.setup(tools.expand_config {
    ["on_attach"] = on_attach,
    ["capabilities"] = lsp_capabilities,
    ["autostart"] = true,
    ["settings.pylsp.plugins.autopep8.enabled"] = false,
    ["settings.pylsp.plugins.yapf.enabled"] = false,
    ["settings.pylsp.plugins.pydocstyle.enabled"] = true,
    ["settings.pylsp.plugins.pydocstyle.convention"] = "google",
    ["settings.pylsp.plugins.pycodestyle.maxLineLength"] = 88, -- black's default
    ["settings.pylsp.plugins.isort.enabled"] = true,
    ["settings.pylsp.plugins.black.enabled"] = true,
})

lspconfig.julials.setup {
    on_attach = on_attach,
    autostart = true,
    capabilities = lsp_capabilities,
    -- cmd = { "julia-lsp" }
}

lspconfig.lua_ls.setup(tools.expand_config {
    ["capabilities"] = lsp_capabilities,
    ["autostart"] = true,
    ["settings.Lua.completion.callSnippet"] = "Replace",
    ["settings.Lua.workspace.checkThirdParty"] = false,
    ["settings.Lua.workspace.library"] = {
        vim.fn.expand("$VIMRUNTIME"),
        require("neodev.config").types(),
    },
    ["settings.globals"] = {
        "vim", -- vim global
        "use", -- Packer use keyword
    },
})

-- NixLang
lspconfig.nixd.setup(tools.expand_config {
    autostart = true,
    on_attach = on_attach,
    capabilities = lsp_capabilities,
    ["settings.nixd.formatting.command"] = { "nixfmt" },
})

-- Other languages
-- ===============
-- lspconfig.sqls.setup({ on_attach = on_attach, autostart = true, capabilities = lsp_capabilities })          -- SQL
lspconfig.rust_analyzer.setup({ on_attach = on_attach, autostart = true, capabilities = lsp_capabilities }) -- rust
lspconfig.dartls.setup({ on_attach = on_attach, autostart = true, capabilities = lsp_capabilities })        -- dart
-- lspconfig.ltex.setup { autostart = true, capabilities = lsp_capabilities } -- many languages, but heavy
lspconfig.texlab.setup({ on_attach = on_attach, autostart = true, capabilities = lsp_capabilities })        -- latex
lspconfig.html.setup({ on_attach = on_attach, autostart = true, capabilities = lsp_capabilities })          -- html
lspconfig.nim_langserver.setup { on_attach = on_attach, autostart = true, capabilities = lsp_capabilities } -- nimble install nimlangserver

-- setup keybindings for LSP
-- =========================
vim.api.nvim_create_autocmd('LspAttach', {
    desc = 'lsp buffer configuration',
    group = vim.api.nvim_create_augroup('LspAutoKeybindings', {}),
    callback = function(event)
        -- completion with lsp's omnifunc
        vim.bo[event.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

        local conform, _ = pcall(require, "conform")
        local opts = { buffer = event.buf }
        vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
        vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
        vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
        vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
        vim.keymap.set('n', 'go', vim.lsp.buf.type_definition, opts)
        vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
        vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
        vim.keymap.set('n', '<leader>r', vim.lsp.buf.rename, opts)
        if not conform then
            vim.keymap.set({ 'n', 'x' }, '<leader>f', vim.lsp.buf.format, opts)
        end
        vim.keymap.set('n', '<F4>', vim.lsp.buf.code_action, opts)
        vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
        vim.keymap.set('n', ']a', vim.diagnostic.goto_prev, opts)
    end
})
