--- color aliases
local active_bg = "fg"
local active_fg = "bg"

-- map color alias to color scheme
-- sample hi command: hi SttFileName ctermbg=2 ctermfg=BLACK
vim.api.nvim_set_hl(0, 'StatusLineItem', { fg = active_fg, bg = active_bg, bold = true })
vim.api.nvim_set_hl(0, 'StatusLineMarker', { fg = active_bg, bg = "NONE", bold = true })
vim.api.nvim_set_hl(0, 'StatusLineBg', { fg = "fg", bg = "bg", bold = true })

--- Apply highlight to text
--- @param s string: the text string
--- @param color string: the highlight name, eg, `Normal`
--- @return string: the highlighted text
local with_hl = function(s, color)
    return '%#' .. color .. '#' .. s .. '%#StatusLineBg#'
end

--- Apply left and right marker with approriate highlight to item
--- @param s string: item string
--- @param left string: left marker string
--- @param right string: right marker string
--- @return string: formatted string
local with_marker = function(s, left, right)
    return table.concat { -- mode indicator
        with_hl(left, 'StatusLineMarker'),
        with_hl(s, 'StatusLineItem'),
        with_hl(right, 'StatusLineMarker'),
    }
end

--- Create a statusline item with marker and highlight
--- @param item string: the item string
--- @param leftmarker string: the left marker string
--- @param rightmarker string: the right marker string
--- @return string: formatted item
local item = function(item, leftmarker, rightmarker)
    local s = with_hl(item, 'StatusLineItem')
    s = with_marker(s, leftmarker, rightmarker)
    return s
end

--- String markup with some condition
--- @param cond string: the condition, example: '&modified'
--- @param s string: the string that appears if satisfy, example: '[+]'
--- @return string: conditioned expression
local condition = function(cond, s)
    local expr = [[%{% ]] .. cond .. [[ ? ']] .. s .. [[' : '' %}]]
    return expr
end


--- Create statusline highlight
--- @return string: statusline string
local function build_status_line()
    -- some unicodes to decorate the line:
    local left = '\u{25e2}'
    local right = '\u{25e4}'
    local parts = {
        item(' [%{toupper(mode())}] ', '', right),         -- mode indicator
        item(' %f ', left, right),                         -- filename
        condition('&modified', item(' %m ', left, right)), -- modified flag
        condition('&readonly', item(' %r ', left, right)), -- modified flag
        with_hl('%=', 'StatusLineBg'),                     -- spacing
        item(' %l/%c ', left, right),                      -- line/column number
        item(' %P/%L ', left, right),                      -- percentage
        item(' %Y ', left, ''),                            -- file type
    }

    return table.concat(parts, '  ')
end

vim.opt.statusline = build_status_line()
