local M = {}

--- Set a chain of nested keys to a value
-- @param tbl: The table to be modified
-- @param keys: The table of keys to set
-- @param value: The value at the leaf node
M.chain_set = function(tbl, keys, value)
	-- number of levels of nested tables
	local depth = #keys

	-- Perverted cases, impossible
	if depth < 1 then return end

	-- Depth is only 1, set the key directly
	if depth == 1 then
		tbl[keys[1]] = value
		return
	end

	-- Nested table, just peel the table and call chain_set again
	-- Since the key might exists or not, we need to re-asign
	-- the sub-table back to the main table
	local key = table.remove(keys, 1)
	local subtbl = tbl[key] or {}
	M.chain_set(subtbl, keys, value)
	tbl[key] = subtbl
end

--- This function helps define nested configurations more easily.
-- This is inspired by nix syntax and the function lenses.
-- For example, the config `a.b.c = true ` will be exanded to `{a = {b = {c = true}}}`.
-- See also `chain_set` and `chain_get`.
-- @param configs The un-expanded config, only the first level of the config will be expanded
-- @return expanded The expanded configurations
function M.expand_config(configs)
	local expanded = {}

	for key, conf in pairs(configs) do
		-- split key by dot
		local parts = vim.split(key, ".", { plain = true })
		M.chain_set(expanded, parts, conf)
	end

	return expanded
end

return M
