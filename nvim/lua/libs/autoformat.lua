local vim = _G.vim
local lsp_formatter = {}
local spawn = require("spawn")
local uv = vim.loop
local print = vim.print

-- Schema:
-- 1: filetype
-- 2: formatter types
-- 3: formatter command
local example_config = [[
local autoformat = require("autoformat")
autoformat.setup({
	auto = true,
	formatters = {
		python = "isort - | black -q -",
		sh = "shfmt -",
		tex = autoformat.lsp_formatter,
		lua = autoformat.lsp_formatter,
	},
})
]]


--- +-----------------+
--- | formatter state |
--- +-----------------+
local state = {
    formatters = {},
    auto = true,
}

local function show_example()
    print(example_config)
end

--- +----------------------------------+
--- | api function for setup formatter |
--- +----------------------------------+
local function setup(args)
    state.auto = args.auto
    state.formatters = args.formatters
end

--- +---------------------------------+
--- | toggle auto formatter on or off |
--- +---------------------------------+
local function toggle(hint, silent)
    silent = silent or false
    if hint == nil then
        state.auto = not state.auto
    else
        state.auto = hint
    end
    if not silent then
        vim.api.nvim_command("echo 'Auto format enabled: " .. tostring(state.auto) .. "'")
    end
end


local format_current_buffer = function()
    local filetype = vim.bo.filetype
    local format_cmd = state.formatters[filetype]

    --- +--------------+
    --- | no formatter |
    --- +--------------+
    if format_cmd == nil then
        return
    end

    --- +---------------+
    --- | lsp formatter |
    --- +---------------+
    if format_cmd == lsp_formatter then
        vim.lsp.buf.format()
        vim.api.nvim_command("echo 'Formatted with LSP Formatter'")
        return
    end

    -- =======================================
    -- | else, use a command based formatter |
    -- =======================================

    --- +------------+
    --- | store view |
    --- +------------+
    local cursor = vim.api.nvim_win_get_cursor(0)

    --- +-----------------------------------------------------+
    --- | copy the current buffer content to a temporary file |
    --- +-----------------------------------------------------+
    local fds = uv.pipe({ nonblock = true }, { nonblock = true })
    local stdin_write = uv.new_pipe()
    local stdin_read = uv.new_pipe()
    local content = vim.api.nvim_buf_get_lines(0, 0, vim.api.nvim_buf_line_count(0), false)
    content = table.concat(content, "\n")
    stdin_write:open(fds.write)
    stdin_write:write(content)
    stdin_write:close()
    stdin_read:open(fds.read)

     -- +-----------------------------------------------------------+
     -- | command should take input from stdin and output to stdout |
     -- +-----------------------------------------------------------+
    spawn.ez_spawn(format_cmd, stdin_read, function(output)
        --- +-----------------------------------+
        --- | collect shell output, close stdin |
        --- +-----------------------------------+
        local err = output.stderr_string
        local out = output.stdout_string
        spawn.close(stdin_read)

         -- +----------------------------------+
         -- | if error notify and does nothing |
         -- +----------------------------------+
        if not (err == nil or err == "" or err == vim.NIL) then
            vim.notify("cannot format buffer, error: " .. tostring(err), vim.log.levels.ERROR)
            return
        end

        --- +-----------------------+
        --- | write to current file |
        --- +-----------------------+
        local current_path = vim.fn.expand("%")
        local curfile = io.open(current_path, "w+")
        if curfile == nil then
            vim.print("Can't write to the current file, something has gone wrong")
            return
        end
        curfile:write(out)
        curfile:close()

        --- +--------------+
        --- | restore view |
        --- +--------------+
        vim.api.nvim_command("noa set nomodified")
        vim.api.nvim_command("e " .. current_path)
        pcall(vim.api.nvim_win_set_cursor, 0, cursor)
    end)
end

local auto_format = function()
    --- +--------------------------+
    --- | check if formatter is on |
    --- +--------------------------+
    if not state.auto then
        return
    end
    format_current_buffer()
end

--- +-------------------------------------+
--- | create vim command and auto command |
--- +-------------------------------------+
vim.api.nvim_create_user_command("AutoFormat", "lua require('autoformat').format()", {})
vim.api.nvim_create_user_command("AutoFormatToggle", "lua require('autoformat').toggle()", {})
vim.api.nvim_create_user_command("AutoFormatExampleConfig", "lua require('autoformat').show_example()", {})
vim.api.nvim_create_autocmd("BufWritePre", { buffer = 0, callback = auto_format })

--- +---------------+
--- | module return |
--- +---------------+
return {
    format = format_current_buffer,
    auto_format = auto_format,
    toggle = toggle,
    setup = setup,
    example_config = example_config,
    show_example = show_example,
    lsp_formatter = lsp_formatter,
}
