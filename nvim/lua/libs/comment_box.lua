local vim = _G["vim"]
local M = {}

local function len(s)
    return vim.api.nvim_strwidth(s)
end

local function get_comment_mark()
    -- +-----------------------+
    -- | find the comment mark |
    -- +-----------------------+
    local filetype = vim.bo.filetype
    local comment_mark = vim.filetype.get_option(filetype, "commentstring")
    comment_mark = string.gsub(comment_mark, "%%s", "")
    comment_mark = string.gsub(comment_mark, "%s", "")

    -- +----------------------+
    -- | escape for searching |
    -- +----------------------+
    local comment_mark_search = comment_mark
    comment_mark_search = string.gsub(comment_mark_search, "%%", "%%%%")
    comment_mark_search = string.gsub(comment_mark_search, "%-", "%%%-")
    return comment_mark, comment_mark_search
end

M.CBoxLine = function()
    -- +-----------------+
    -- | Get buffer info |
    -- +-----------------+
    local linenr = vim.api.nvim_win_get_cursor(0)[1]
    local curline = vim.api.nvim_buf_get_lines(0, linenr - 1, linenr, false)[1]
    local filetype = vim.bo.filetype

    -- +-----------------------+
    -- | find the comment mark |
    -- +-----------------------+
    local comment_mark = vim.filetype.get_option(filetype, "commentstring")
    comment_mark = string.gsub(comment_mark, "%%s", "")
    comment_mark = string.gsub(comment_mark, "%s", "")
    if comment_mark == "" then
        vim.print("Unknown comment mark")
        return
    end

    -- +----------------------------------------------------------------------------+
    -- | special case, comment starts with %, this will be used by search functions |
    -- +----------------------------------------------------------------------------+
    local comment_mark_search = comment_mark
    comment_mark_search = string.gsub(comment_mark_search, "%%", "%%%%")
    comment_mark_search = string.gsub(comment_mark_search, "%-", "%%%-")

    -- +-----------------------------+
    -- | Check comment mark position |
    -- +-----------------------------+
    local pos = curline:find(comment_mark_search)
    if pos == nil then
        vim.print("Not a comment")
        return
    end

    -- +--------------------+
    -- | Make a comment box |
    -- +--------------------+
    local spaces = curline:sub(1, pos):gsub(comment_mark_search, "") .. comment_mark
    local content = curline:gsub("^%s*" .. string.rep(".", #comment_mark) .. "%s*", "")
    content = content:gsub("%s*#", "")
    local content_length = vim.api.nvim_strwidth(content)
    local new_comment = spaces .. " | " .. content .. " |"
    local border = spaces .. " +-" .. string.rep("-", content_length) .. "-+"

    -- +----------------------------+
    -- | put the line in the buffer |
    -- +----------------------------+
    vim.api.nvim_buf_set_lines(0, linenr - 1, linenr, true, { new_comment })
    vim.api.nvim_put({ border }, "l", false, true)
    vim.api.nvim_put({ border }, "l", true, false)
    vim.api.nvim_command("noautocmd write")
    -- vim.print({
    -- 	spaces,
    -- 	content = content,
    -- 	curline = curline,
    -- 	border = border,
    -- 	pos = pos,
    -- 	comment_mark_search = comment_mark_search,
    -- })
end

local function create_box(lines)
    -- @Args:
    -- `lines`: A list of lines (str).

    --- +----------------------------------+
    --- | Find max length and space length |
    --- +----------------------------------+
    local _, comment_mark_search = get_comment_mark()
    local space_pattern = "^(%s*" .. comment_mark_search .. "%s*)"
    local max_len = 0
    local spaces = nil
    for _, line in ipairs(lines) do
        max_len = math.max(max_len, len(line))
        if spaces == nil then
            _, _, spaces = string.find(line, space_pattern)
        else
            local next_spaces
            _, _, next_spaces = line:find(space_pattern)
            assert(next_spaces == spaces, "The indent level are not the same")
        end
    end

    --- +-------------------+
    --- | no space is found |
    --- +-------------------+
    spaces = spaces or ""

    --- +---------------------------------------+
    --- | remove space lenght from total length |
    --- +---------------------------------------+
    max_len = max_len - len(spaces)

    -- +---------------------+
    -- | remove white spaces |
    -- +---------------------+
    local content_lines = vim.tbl_map(function(line)
        return line:gsub(spaces, "")
    end, lines)

    -- +-------------------------+
    -- | padding + re-add spaces |
    -- +-------------------------+
    content_lines = vim.tbl_map(function(line)
        local n = len(line)
        local padding = string.rep(" ", max_len - n)
        line = spaces .. "| " .. line .. padding .. " |"
        return line
    end, content_lines)

    -- +-------------+
    -- | make border |
    -- +-------------+
    local border_line = spaces .. "+" .. string.rep("-", max_len + 2) .. "+"
    table.insert(content_lines, 1, border_line)
    table.insert(content_lines, border_line)
    return content_lines
end

M.CBoxSel = function()
    local vstart = vim.api.nvim_buf_get_mark(0, "<")
    local vend = vim.api.nvim_buf_get_mark(0, ">")
    local line_start = vstart[1]
    local line_end = vend[1]
    local lines = vim.api.nvim_buf_get_lines(0, line_start - 1, line_end, false)
    if #lines == 0 then
        return
    end
    lines = create_box(lines)
    vim.api.nvim_buf_set_lines(0, line_start - 1, line_end, true, lines)
end


-- +------------------------------------------+
-- | Create the user command for easy calling |
-- +------------------------------------------+
vim.api.nvim_create_user_command("CBox", "lua require('libs.comment_box').CBoxLine()", {})
vim.api.nvim_create_user_command("CBoxSel", "lua require('libs.comment_box').CBoxSel()", { range = 1 })

return M
