local trim = vim.trim

local abs = function(x)
    if x > 0 then
        return x
    else
        return -x
    end
end

local function hardwrap_text(s_, n)
    local s = s_:gsub("\n", " ")
    local lines = {}
    local line = ""

    -- splits
    local over_badness = 3
    local under_badness = 2
    for _, word in ipairs(vim.split(s, " ")) do
        local next = vim.trim(line .. " " .. word)
        local nnext = #next
        local badness = (nnext - n)
        if (-under_badness < badness and badness <= over_badness) or nnext > (n + over_badness) then
            table.insert(lines, next)
            -- vim.print({ line, badness, nnext })
            line = ""
        else
            line = vim.trim(next)
        end
    end

    -- left over
    if #line > 0 then
        table.insert(lines, vim.trim(line))
    end
    return lines
end

vim.api.nvim_create_user_command("Wrap", function(opts)
    -- Collect text
    local line1 = opts.line1
    local line2 = opts.line2
    local lines = vim.api.nvim_buf_get_lines(0, line1 - 1, line2, true)
    -- vim.print(lines)
    local s = ""
    for _, line in ipairs(lines) do
        s = s .. line .. " "
    end

    -- wrapped
    local e_pos = vim.fn.getpos(".")
    -- vim.print(e_pos)
    local wlines = hardwrap_text(s, 90)
    table.insert(wlines, "")
    -- vim.print({ line1 = line1, line2 = line2, e_pos })
    vim.api.nvim_buf_set_text(0, line1 - 1, 0, line2, e_pos[3] - 1, wlines)
end, { range = true })
