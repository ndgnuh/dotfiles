local config_path = vim.fn.stdpath("data")
local rel = function(file)
    return vim.fs.joinpath(config_path, file)
end

--- bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

--- lazy.nvim options
local opts = {}


require("lazy").setup({
    -- fuzzy finder
    {
        "nvim-telescope/telescope.nvim",
        tag = "0.1.5",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-tree/nvim-web-devicons",
        },
    },

    -- editor shortcut misc
    "tpope/vim-surround",
    "terrortylor/nvim-comment",
    {
        "godlygeek/tabular",
        cmd = "Tabularize",
    },

    -- colorscheme, has to be loaded after, idk why
    "morhetz/gruvbox",
    "nyoom-engineering/oxocarbon.nvim",
    "aktersnurra/no-clown-fiesta.nvim",
    "nvim-tree/nvim-web-devicons",

    -- language support, julia ext is still needed for unicode latex
    -- setting ft = 'Julia' causes error, see also: https://github.com/JuliaEditorSupport/julia-vim/issues/269
    {
        -- 'JuliaEditorSupport/julia-vim',
        "ndgnuh/julia-vim",
        ft = { "julia" },
    },
    { "hiphish/jinja.vim" },

    -- LSP
    "neovim/nvim-lspconfig",
    "williamboman/mason.nvim",
    "williamboman/mason-lspconfig.nvim",

    { -- completion
        "hrsh7th/nvim-cmp",
        version = false,
        event = "InsertEnter",
        dependencies = {
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-path",
        },
    },

    {
        "L3MON4D3/LuaSnip",
        lazy = true,
        dependencies = {
            "rafamadriz/friendly-snippets",
            -- "mireq/luasnip-snippets",
            'saadparwaiz1/cmp_luasnip',
        },
    },

    -- { -- outline
    --     "hedyhli/outline.nvim",
    --     lazy = true,
    --     cmd = { "Outline", "OutlineOpen" },
    --     keys = { -- Example mapping to toggle outline
    --         { "<leader>o", "<cmd>Outline<CR>", desc = "Toggle outline" },
    --     },
    --     dependencies = {
    --         -- { "nvim-treesitter/nvim-treesitter", branch = "master" },
    --         "nvim-tree/nvim-web-devicons",
    --     },
    --     opts = {},
    -- },

    -- formatter
    "stevearc/conform.nvim",

    -- better fold?
    -- { 'kevinhwang91/nvim-ufo',           dependencies = { 'kevinhwang91/promise-async' } },

    -- dev tools
    { "folke/neodev.nvim" },

    -- tree sitter
    { "nvim-treesitter/nvim-treesitter", run = vim.cmd.TSUpdate, branch = "master" },

    -- tree sitter code outline, use when the outline is bugged, somehow...
    -- {
    --     'stevearc/aerial.nvim',
    --     opts = {},
    --     -- Optional dependencies
    --     dependencies = {
    --         "nvim-treesitter/nvim-treesitter",
    --         "nvim-tree/nvim-web-devicons"
    --     },
    -- }

    -- docstring helper, auto wrap text
    {
        "andrewferrier/wrapping.nvim",
        config = function()
            require("wrapping").setup()
        end
    },
}, opts)

-- one-liner setups
require("neodev").setup({}) -- setup lua dev tools
require("nvim_comment").setup({})
require("nvim-web-devicons").setup({})
