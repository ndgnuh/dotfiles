local options
-- Docs:
-- help command-completion
-- help nvim_create_user_command
options = { desc = "Rename the current buffer to something else" , nargs = 1 }
vim.api.nvim_create_user_command("Rename", function(args)
	local buf_path = vim.api.nvim_buf_get_name(0)
	local dir_name = vim.fs.dirname(buf_path)
	local base_name = vim.fs.basename(buf_path)
	local new_name = vim.fs.joinpath(dir_name, args.args)
	if buf_path == "" then
		error("\nCurrent buffer does not have a name")
	end
	os.rename(buf_path, new_name)
	vim.cmd.edit(new_name)
end, options)
