-- numbering
vim.opt.number = true

-- setup indentation
vim.opt.shiftwidth = 4
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.expandtab = true

-- use system clipboard if available
vim.opt.clipboard = "unnamedplus"

-- remove search highlight but highlight the parts being searched, live
vim.opt.hlsearch = false
vim.opt.incsearch = true

-- line wraps with linebreak is MUCH better
vim.opt.wrap = true
vim.opt.linebreak = true

-- no completion preview
vim.opt.completeopt = 'menu'

-- use tree sitter for folding
-- also open all the fold by default
vim.opt.foldexpr = "v:lua.vim.treesitter.foldexpr()"
-- vim.opt.foldtext = "v:lua.vim.treesitter.foldtext()"
vim.opt.foldmethod = "expr"
vim.opt.foldlevel = 99 -- Disable folding by default be cause it auto folds everytime we exit normal mode, wtf
vim.opt.foldopen = "hor,mark,percent,quickfix,search,tag,undo"

-- local configuration
vim.opt.exrc = true
