vim.keymap.set('n', '<leader>so', vim.cmd.so, {})
vim.keymap.set({ 'i', 'n' }, '<C-c>', '<Esc>', {})
vim.keymap.set('x', 'ga', ':Tabularize/', {})
vim.keymap.set('x', '<leader>w', ':Wrap<CR>', {})
