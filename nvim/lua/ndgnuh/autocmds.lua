local autofiletype = vim.api.nvim_create_augroup('AutoFileType', {})

vim.api.nvim_create_autocmd("BufRead", {
    pattern = "*.bbx,*cbx,*.cls,*.sty,*.lbx",
    group = autofiletype,
    callback = function(event)
        local buf = event.buf
        vim.opt_local.filetype = "tex"
    end
})

-- vim.api.nvim_create_autocmd("BufRead", {
--     pattern = ".sh",
--     group = autofiletype,
--     callback = function(event)
--         local opts = { buffer = event.buf }
--         vim.keymap.set('n', '<F5>', function()
--             os.cmd()
--         end, opts)
--     end
-- })

vim.api.nvim_create_autocmd("BufRead", {
    pattern = "*.jl",
    group = autofiletype,
    callback = function(event)
        vim.opt_local.syntax = "julia"
    end
})

-- vim.api.nvim_create_autocmd("FileType", {
--     pattern = "tex",
--     group = autofiletype,
--     callback = function(...)
--         vim.opt_local.foldmarker = ">>>,<<<"
--         vim.opt_local.foldmethod = "marker"
--     end
-- })
