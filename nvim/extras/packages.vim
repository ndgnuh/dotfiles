" =========================
" Vim extensions using Plug
" =========================
let s:plug_path = stdpath('cache') . '/plugged'
call plug#begin(s:plug_path)
" Searching files
Plug 'junegunn/fzf', { 'dir': '~/.cache/.fzf', 'do': './install --all', 'on': 'FZF'}

" Surrounding targets, braces, parentheses, etc.
Plug 'tpope/vim-surround'

" Align characters for table-like formatting
Plug 'godlygeek/tabular', { 'on': 'Tabularize' }

" Color scheme, very important
Plug 'morhetz/gruvbox'

" Commenting lines
Plug 'terrortylor/nvim-comment'

" Language supports
Plug 'sheerun/vim-polyglot' " Generic, for a lot of file types
Plug 'JuliaEditorSupport/julia-vim', { 'for': 'julia' }
Plug 'kdheepak/JuliaFormatter.vim', { 'for': 'julia' }
Plug 'ElmCast/elm-vim', { 'for': 'elm' } " Elm web lang
Plug 'elkowar/yuck.vim', { 'for': 'yuck' } " Eww config lang
Plug 'neoclide/vim-jsx-improve', { 'for': ['html', 'css', 'js', 'jsx'] }

" LSP packages
Plug 'neovim/nvim-lspconfig' " Ez configuration for LSP
Plug 'ray-x/lsp_signature.nvim' " Show signature hints when typing

" Dev tools, for Vim
Plug 'folke/neodev.nvim'
call plug#end()



" For some reason, setting the colorscheme inside plug won't work
" Therefore we set them outside
colorscheme gruvbox
hi Normal ctermbg=NONE " Use terminal background
