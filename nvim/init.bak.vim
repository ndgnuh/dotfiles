" ===========
" Vim options
" ===========
set nocompatible
filetype indent on
syntax on
set number
if !(empty($DISPLAY))
	set clipboard=unnamedplus
endif
set shiftwidth=4
set tabstop=4
set softtabstop=4
set notermguicolors
set linebreak
set completeopt=menu,menuone,noselect


" =====================
" Custom Vim extensions
" =====================
function s:source(file)
	execute 'source ' . stdpath('config') . '/' . a:file
endfunction
call s:source('extras/plug-install.vim') " Function to install VimPlug
try
	call s:source('extras/packages.vim') " Load extra packages
	call s:source('extras/ibus.vim') " Automatic switch IBus engine between modes
	call s:source('extras/statusline.vim') " Custom statusline
	lua require('nvim_comment').setup{ comment_empty = false } -- commenting shortcuts
	lua require("neodev").setup{} -- setup lua dev tools
	lua require('libs.comment_box') -- wrap comments in boxes
	lua require('libs.formatter') -- format codes
	lua require('my_lspconfig') -- My LSP configurations
catch
	echo "Vim plug is not installed, won't load packages"
endtry



" ==================
" Custom keybindings
" ==================
" * Reload config
nnoremap <leader>r <cmd>so<CR>
" * Remove highlight
nnoremap <Esc> <cmd>noh<CR>
" * FZF search file
nnoremap <C-p> <cmd>FZF<CR>
" * Align codes, this command requires input, thus does not begin with <cmd>
xnoremap ga :Tabularize/
" Auto format code using LSP
nnoremap <leader>f <cmd>lua AutoFormat()<cr>
