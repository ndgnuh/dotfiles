{
  lib,
  rclone,
  ...
}:
let

  default-flags = [
    "--dir-cache-time 48h"
    "--vfs-cache-mode full"
    "--vfs-cache-max-age 48h"
    "--vfs-read-chunk-size 10M"
    "--vfs-read-chunk-size-limit 512M"
    "--buffer-size 512M"
  ];
  # mkdir = /run/current-system/sw/bin/mkdir;

  # Use system's rclone
  rcloneExec = "/bin/rclone";
  fusermountExec = "/bin/fusermount";
  mkdir = "mkdir";
in
{
  default-flags = default-flags;

  rcloud-unit =
    {
      what,
      where,
      flags ? default-flags,
    }:
    let
      flag_str = lib.concatStrings (lib.intersperse " " flags);
    in
    {
      Unit = {
        Description = "Rclone mount ${what} to ${where}";
        After = [ "network-online.service" ];
        Requires = [ "network-online.service" ];
      };
      Install.WantedBy = [ "default.target" ];
      Service = {
        ExecStartPre = ''${mkdir} -p ${where}'';
        ExecStart = ''
          ${rcloneExec} mount ${what} ${where} ${flag_str}
        '';
        ExecStop = "${fusermountExec} -u ${where}";
        Type = "simple";
        Environment = [ "PATH=/run/wrappers/bin/:$PATH" ]; # Required environments
        TimeoutStartSec = 10; # Maximum 10 sec before killed?
        # RuntimeMaxSec = 10; # Maximum 10 sec before killed?
      };
    };
}
