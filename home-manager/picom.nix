{
  config,
  pkgs,
  lib,
  ...
}:
{
  systemd.user.services.picom = {
    Unit = {
      Description = "Picom X11 compositor";
      After = [ config.systemd.target ];
      PartOf = [ config.systemd.target ];
    };

    Install = {
      WantedBy = [ config.systemd.target ];
    };

    Service = {
      ExecStart = lib.concatStringsSep " " [
        "${lib.getExe config.package}"
        "--config ${config.xdg.configFile."picom/picom.conf".source}"
      ];
    };
  };
}
