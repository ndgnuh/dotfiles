{
  config,
  lib,
  pkgs,
  ...
}:

let
  # Create cloud mounting services using rclone
  rcloud = pkgs.callPackage ./rcloud.nix { };
in
{
  # Allow unfree stuff
  nixpkgs = {
    config = {
      allowUnfree = true;
      allowUnfreePredicate = (_: true);
    };
    overlays = [
      (import ./overlays/python3-env)
      (import ./overlays/julia.nix)
      (import ./overlays/texlive-env.nix)
      (import ./overlays/sakura.nix)
      (import ./overlays/distro-grub-theme-nixos.nix)
    ];
  };

  # Home Manager needs a bit of information about you and the paths it should
  home.username = "hung";
  home.homeDirectory = "/home/hung";

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "24.11"; # Please read the comment before changing.

  # The home.packages option allows you to install Nix packages into your
  # environment.
  # home.packages = [];
  home.packages = with pkgs; [
    # Examples:
    # (pkgs.nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })
    # (pkgs.writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')

    # system tools
    xdotool
    powershell
    hw-probe
    cargo
    gparted
    parted
    pciutils
    htop
    gnupg
    git
    zsh
    neovim
    file
    tmux
    feh
    nmap
    yt-dlp
    mediainfo
    ffmpeg-full
    imagemagickBig
    ghostscript_headless
    fzf
    lm_sensors
    psmisc
    inxi
    zcfan
    docker
    distrobox
    nix-index
    mesa-demos
    lsb-release
    hardinfo
    neofetch
    aria2
    ventoy-full
    nix-init

    # Android
    android-tools
    adbfs-rootless
    android-studio-tools

    # Inxi reccommends
    bluez-tools
    dmidecode
    doas
    system-config-printer
    hddtemp
    usbutils
    mdadm
    busybox
    smartmontools
    tree
    vulkan-tools
    vulkan-loader
    wayland-utils
    wmctrl
    xorg.xdriinfo
    entr
    gnumake
    cmake

    # XDG tools
    xdgmenumaker
    xdg-utils
    xdg-user-dirs
    xdg-desktop-portal-gtk
    xdg-desktop-portal
    lxqt.xdg-desktop-portal-lxqt
    dex

    # Filesystem and Synchronization
    # rsync
    ncdu
    # stow
    # rclone
    syncthing
    wget
    curl
    localsend
    # fuse
    # fuse3
    # fuseiso
    # sshfs
    # bindfs

    # Tex stuffs
    texlive-env
    lyx
    texstudio
    qpdfview
    xreader
    poppler_utils

    # Themes
    yaru-theme
    materia-theme
    materia-kde-theme
    vimix-icon-theme
    vimix-cursors
    # vimPlugins.nvim-web-devicons

    # Music
    mpd
    mpdris2
    mpd-mpris
    cantata
    mpc-cli
    ncmpcpp
    playerctl

    # Xorg stuffs
    xorg.xdpyinfo
    xorg.xev
    xorg.xkill
    xorg.xhost
    xorg.xinit
    xorg.xeyes
    xsettingsd
    xclip
    arandr
    copyq
    pavucontrol
    # gobject-introspection
    copyq
    libnotify
    xdragon
    redshift

    # Language servers
    texlab
    nixd
    nix-plugins
    nixfmt-rfc-style
    yaml-language-server
    vue-language-server
    vim-language-server
    rust-analyzer # Rust LSP
    biome
    rustfmt # Rust Formatter
    clippy # Rust Linter
    matlab-language-server
    lua-language-server
    jdt-language-server
    elmPackages.elm-language-server
    dockerfile-language-server-nodejs
    nodePackages.bash-language-server
    vim-language-server
    sql-formatter
    sqls
    vscode-langservers-extracted
    tree-sitter

    # Archives
    p7zip
    unzip
    zip
    rar
    # unrar
    xar

    # Languages and runtimes
    typst
    typstyle
    bun
    octaveFull
    gcc
    # geckodriver # For selenium
    # selenium-manager
    # selenium-server-standalone
    # jupyter
    python3-env
    flutter
    maxima
    wxmaxima
    julia-build-sysimage
    sqlitebrowser
    gibo # Generate git ignore
    nodejs

    # Terminals
    lxde.lxappearance
    sakura
    dmenu
  ];

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    ".zshrc".source = ../home/.zshrc;
    ".zshenv".source = ../home/.profile;
    ".profile".source = ../home/.profile;
    ".xsessionrc".source = ../home/.profile;
    ".config/sakura".source = ../sakura;
    ".config/picom".source = ../picom;

    # Frequently changed
    # out-of-store symlink makes the file read-writeable
    # more importantly, it makes direct symlink, any changes
    # will be reflected to the central dot repository
    ".config/nvim".source = config.lib.file.mkOutOfStoreSymlink ../nvim;
    ".config/ibus-bamboo".source = config.lib.file.mkOutOfStoreSymlink ../ibus-bamboo;
    ".config/awesome".source = config.lib.file.mkOutOfStoreSymlink ../awesome;
  };

  # Home Manager can also manage your environment variables through
  # 'home.sessionVariables'. These will be explicitly sourced when using a
  # shell provided by Home Manager. If you don't want to manage your shell
  # through Home Manager then you have to manually source 'hm-session-vars.sh'
  # located at either
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  ~/.local/state/nix/profiles/profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/hung/etc/profile.d/hm-session-vars.sh
  #
  home.sessionVariables = {
    EDITOR = "nvim";
    BROWSER = "firefox";
    PATH = "$PATH:$HOME/.pub-cache/bin";
  };

  # GTK configuration
  gtk =
    let
      extraCss = "vte-terminal { padding: 1em; }";
    in
    {
      enable = true;
      cursorTheme.package = pkgs.vimix-cursors;
      cursorTheme.name = "Vimix";
      iconTheme.package = pkgs.vimix-icon-theme;
      iconTheme.name = "Vimix";
      theme.name = "Yaru";
      theme.package = pkgs.yaru-theme;
      gtk3.extraCss = extraCss;
      gtk4.extraCss = extraCss;
      gtk3.bookmarks = [
        "file:///home/${config.home.username}/Documents"
        "file:///home/${config.home.username}/Pictures"
        "file:///home/${config.home.username}/Downloads"
        "file:///home/${config.home.username}/Music"
        "file:///home/${config.home.username}/Movies"
        "file:///home/${config.home.username}/MyTmp Local Temp"
        "file:///tmp System Temp"
        "file:///home/${config.home.username}/Projects"
        "file:///home/${config.home.username}/Projects/HUST-GD HUST Giảng dạy"
        "file:///home/${config.home.username}/Projects/HUST-NC HUST Nghiên cứu"
        "file:///home/${config.home.username}/Projects/HUST-PV HUST Phục vụ"
        "file:///home/${config.home.username}/Documents/Manuals/latex/ Latex Manuals"
        "file:///home/${config.home.username}/Projects/HUST-PV/Urgent/"

      ];
    };

  # QT configuration
  qt = {
    enable = true;
    platformTheme.name = "qt5ct";
  };

  # Git configuration
  programs.git = {
    enable = true;
    lfs.enable = true;
    userName = "Hung Nguyen Duc";
    userEmail = "ndgnuh@protonmail.com";
  };

  # MPD
  services.mpdris2.enable = true;
  services.mpd = {
    enable = true;
    musicDirectory = ~/Music;
    playlistDirectory = ~/.cache/mpd/playlist;
    dataDir = ~/.cache/mpd;
    dbFile = "~/.cache/mpd/database";
  };

  # Compositors
  #   services.picom = {
  #     enable = true;
  #     vSync = true;
  #     fade = true;
  #     shadow = false;
  #     fadeDelta = 10;
  #     fadeSteps = [
  #       5.0e-2
  #       5.0e-2
  #     ];
  #     settings = {
  #       animations = ''
  # (
  #     { preset = "appear"; triggers = [ "open" ]; },
  #     { preset = "disappear"; triggers = [  "close" ]; },
  #     { direction = "down"; preset = "fly-out"; triggers = [  "hide" ]; },
  #     { direction = "down"; preset = "fly-in"; triggers = [  "show" ]; },
  # );
  #       ''
  #     };
  #   };

  # XDG directories
  xdg.userDirs = {
    createDirectories = true;
    enable = true;
    desktop = "$HOME/Desktop";
    download = "$HOME/Downloads";
    templates = "$HOME/Templates";
    publicShare = "$HOME/Public";
    documents = "$HOME/Documents";
    music = "$HOME/Music";
    pictures = "$HOME/Pictures";
    videos = "$HOME/Movies";
    extraConfig = {
      projects = "$HOME/Projects";
    };
  };

  # Syncthing
  services.syncthing.enable = true;

  # XSettings
  services.xsettingsd = {
    enable = true;
    settings = {
      "Net/ThemeName" = "Yaru";
      "Net/IconThemeName" = "Vimix";
    };
  };
  xresources.properties = {
    "Xft.dpi" = "144";
    "Xcursor.theme" = "Vimix Cursors";
    "Xcursor.size" = "48";
    "Xft.hinting" = "1";
    "Xft.hintstyle" = "hintfull";
    "Xft.lcdfilter" = "lcddefault";
    "Xft.rgba" = "rgb";
  };

  # Bind directories
  # Run systemd-escape --path "$where" to get the keys
  systemd.user.mounts =
    let
      HOME = config.home.homeDirectory;
      USER = config.home.username;
    in
    {
      "home-${USER}-Projects-HUST\\x2dGD" = {
        Unit.Description = "Teaching materials (HUST-GD)";
        Install.WantedBy = [ "default.target" ];
        Mount = {
          What = /${HOME}/Documents/HUST-GD;
          Where = /${HOME}/Projects/HUST-GD;
          Type = "fuse.bindfs";
        };
      };
    };

  # Wait for network, needed for cloud mounting
  # https://unix.stackexchange.com/questions/216919/how-can-i-make-my-user-services-wait-till-the-network-is-online
  systemd.user.services.network-online = {
    Unit.Description = "Waiting for network";
    Install.WantedBy = [ "default.target" ];
    Service = {
      Type = "OneShot";
      ExecStart = "/run/current-system/sw/lib/systemd/systemd-networkd-wait-online";
      RemainAfterExit = "yes";
    };
  };

  # Custom services
  systemd.user.services = {
    picom = {
      Unit = {
        Description = "Picom X11 compositor";
        After = [ "default.target" ];
        PartOf = [ "default.target" ];
      };

      Install = {
        WantedBy = [ "default.target" ];
      };

      Service = {
        ExecStart = ''${pkgs.picom}/bin/picom'';
      };
    };
    gdrive-mount = rcloud.rcloud-unit {
      what = "GoogleDrive:";
      where = "${config.home.homeDirectory}/Sync/GoogleDrive";
    };
    dropbox-mount = rcloud.rcloud-unit {
      what = "DropBox:";
      where = "${config.home.homeDirectory}/Sync/DropBox";
    };
    onedrive-mount = rcloud.rcloud-unit {
      what = "OneDrive:";
      where = "${config.home.homeDirectory}/Sync/OneDrive";
    };
  };

  # Run commands after building, for now, it only
  # copy corefonts over so that only office can see it...
  home.activation =
    let
      fontdir = "${config.home.homeDirectory}/.local/share/fonts/corefonts/";
    in
    {
      corefonts = lib.hm.dag.entryAfter [ ] ''
        rm -rf ${fontdir}
        mkdir -p ${fontdir}
        cp ${pkgs.corefonts}/share/fonts/truetype/* -r ${fontdir}
      '';
    };

  # Tmux
  programs.tmux = {
    enable = true;
    escapeTime = 10;
    keyMode = "emacs";
    mouse = true;
    extraConfig = ''
      set -gw xterm-keys on
      bind-key h split -h
      bind-key v split -v
      bind-key r source-file ~/.tmux.conf
    '';
  };

  # Let Home Manager install and manage itself.
  # home.enableNixpkgsReleaseCheck = false;
  programs.home-manager.enable = true;
}
