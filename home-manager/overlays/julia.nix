final: prev: {
  julia-custom = prev.julia-bin.overrideAttrs (old: rec {
    version = "1.11.2";
    src = prev.fetchurl {
      url = "https://julialang-s3.julialang.org/bin/linux/x64/${prev.lib.versions.majorMinor version}/julia-${version}-linux-x86_64.tar.gz";
      sha256 = "sha256-ijcq0mLU1NVaEET0/jvOfJpKPOjFE9JHDljoBx7s1HY=";
    };

    installCheckPhase = ''
      echo "life is too short for testing"
      echo "and I don't have a server rig"
    '';
  });

  # Julia script: build system image
  # ================================
  # Build the system image of all the packages
  # in the current project
  julia-build-sysimage = prev.writeShellScriptBin "julia-build-sysimage" ''
    julia -e "
    using Pkg
    Pkg.activate(; temp=true)
    Pkg.add(\"PackageCompiler\")
    using PackageCompiler
    if isfile(\"sys.so\")
        @info \"File sys.so exists, delete it first and rerun the script to rebuild\"
    else
        create_sysimage(; sysimage_path=\"sys.so\")
    end
    "
  '';
}
