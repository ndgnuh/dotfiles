final: prev:
let
  python3Packages = final.python311Packages;
  pypkg =
    {
      pname,
      version,
      hash,
      dependencies ? [ ],
    }:
    python3Packages.buildPythonPackage {
      inherit pname version dependencies;
      src = final.fetchPypi {
        inherit pname version hash;
      };
    };

  lenses = pypkg {
    pname = "lenses";
    version = "1.1.0";
    hash = "sha256-yQWDP4ktzvRd5EEaQ/H/MJN3SBV7arUYH8oGO7CVzoE=";
  };
  manimgl = pypkg {
    pname = "manimgl";
    version = "1.7.2";
    hash = "sha256-WkZy/zg0khhxx5oINGLepwalfLWi6IA6SPVTPQ9tGVY=";
    dependencies = with python3Packages; [
      addict
      appdirs
      colour
      diskcache
      fonttools
      ipython
      isosurfaces
      manimpango
      mapbox-earcut
      matplotlib
      moderngl
      moderngl-window
      numpy
      pillow
      pydub
      pygments
      pyopengl
      pyperclip
      pyyaml
      rich
      scipy
      screeninfo
      skia-pathops
      svgelements
      sympy
      tqdm
      typing-extensions
      validators
    ];
  };
in
{
  # do not overlay python version
  # inherit python3 python3Packages;
  python3-env = prev.python311.withPackages (
    p: with p; [
      jupyter
      virtualenv
      ilua
      nix-kernel

      gdown
      lenses
      pyrsistent
      jinja2
      fastapi
      uvicorn
      pydantic
      sqlalchemy

      polars
      pandas
      numpy
      scipy
      matplotlib
      seaborn
      scikit-learn
      onnxruntime
      pillow
      opencv-python
      cython
      manimgl

      pynvim
      pytest
      mypy
      python-lsp-server
      pyls-isort
      isort
      black
      python-lsp-black
      jedi-language-server

      selenium
      undetected-chromedriver
    ]
  );
}
