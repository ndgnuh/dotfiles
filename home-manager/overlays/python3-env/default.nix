final: prev:
let
  importPkg =
    pkgPath:
    import pkgPath {
      inherit (final) lib fetchPypi fetchFromGithub;
      python3 = final.python3;
    };

  extra_packages = [
    (importPkg ./python3-manim.nix)
    (importPkg ./python3-lenses.nix)
    (importPkg ./python3-manimgl.nix)
    (importPkg ./python3-pyecharts.nix)
    # (importPkg ./python3-vbuild.nix)
    # (importPkg ./python3-nicegui.nix)
  ];
in
{
  # do not overlay python version
  # inherit python3 python3Packages;
  python3-env = prev.python311.withPackages (
    p:
    with p;
    [
      jupyter
      virtualenv
      ilua
      nix-kernel

      gdown
      pyrsistent
      jinja2
      fastapi
      uvicorn
      pydantic
      sqlalchemy

      polars
      pandas
      numpy
      scipy
      matplotlib
      seaborn
      scikit-learn
      onnxruntime
      pillow
      opencv-python
      cython

      icecream
      pynvim
      pytest
      mypy
      python-lsp-server
      pyls-isort
      isort
      black
      python-lsp-black
      jedi-language-server
      pygments

      pyautogui
      selenium
      undetected-chromedriver
    ]
    ++ extra_packages
  );
}
