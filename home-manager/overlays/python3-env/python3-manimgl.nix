{
  lib,
  python3,
  fetchPypi,
  ...
}:

python3.pkgs.buildPythonApplication rec {
  pname = "manimgl";
  version = "1.7.2";
  pyproject = true;

  src = fetchPypi {
    inherit pname version;
    hash = "sha256-WkZy/zg0khhxx5oINGLepwalfLWi6IA6SPVTPQ9tGVY=";
  };

  build-system = [
    python3.pkgs.setuptools
    python3.pkgs.wheel
  ];

  dependencies = with python3.pkgs; [
    addict
    appdirs
    colour
    diskcache
    fonttools
    ipython
    isosurfaces
    manimpango
    mapbox-earcut
    matplotlib
    moderngl
    moderngl-window
    numpy
    pillow
    pydub
    pygments
    pyopengl
    pyperclip
    pyyaml
    rich
    scipy
    screeninfo
    skia-pathops
    svgelements
    sympy
    tqdm
    typing-extensions
    validators
  ];

  pythonImportsCheck = [
  ];

  meta = {
    description = "Animation engine for explanatory math videos";
    homepage = "https://pypi.org/project/manimgl/";
    license = lib.licenses.mit;
    maintainers = with lib.maintainers; [ ];
    mainProgram = "manimgl";
  };
}
