{
  lib,
  python3,
  fetchFromGitHub,
  ...
}:

python3.pkgs.buildPythonApplication rec {
  pname = "vbuild";
  version = "0.8.2";
  pyproject = true;

  src = fetchFromGitHub {
    owner = "manatlan";
    repo = "vbuild";
    rev = "v${version}";
    hash = "sha256-p9v1FiYn0cI+f/25hvjwm7eb1GqxXvNnmXBGwZe9fk0=";
  };

  build-system = [
    python3.pkgs.poetry
    python3.pkgs.poetry-core
  ];

  dependencies = with python3.pkgs; [
    pscript
  ];

  pythonImportsCheck = [
    "vbuild"
  ];

  meta = {
    description = "Compile\" your VueJS components (sfc/*.vue) to standalone html/js/css ... python only (no need of nodejs). Support python components too";
    homepage = "https://github.com/manatlan/vbuild";
    changelog = "https://github.com/manatlan/vbuild/blob/${src.rev}/changelog.md";
    license = lib.licenses.mit;
    maintainers = with lib.maintainers; [ ];
    mainProgram = "vbuild";
  };
}
