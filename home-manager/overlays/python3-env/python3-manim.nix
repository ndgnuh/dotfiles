{
  lib,
  python3,
  fetchPypi,
  ...
}:
python3.pkgs.buildPythonPackage rec {
  pname = "manim";
  version = "0.18.1";
  pyproject = true;

  src = fetchPypi {
    inherit pname version;
    hash = "sha256-S/K0edJYtBAlnGgoJh/nnhB7648t0E6/pzuWvO/d6T0=";
  };

  build-system = [
    python3.pkgs.poetry-core
    python3.pkgs.setuptools
  ];

  dependencies = with python3.pkgs; [
    click
    cloup
    decorator
    importlib-metadata
    isosurfaces
    manimpango
    mapbox-earcut
    moderngl
    moderngl-window
    networkx
    numpy
    pillow
    pycairo
    pydub
    pygments
    rich
    scipy
    screeninfo
    skia-pathops
    srt
    svgelements
    tqdm
    typing-extensions
    watchdog
  ];

  optional-dependencies = with python3.pkgs; {
    gui = [
      dearpygui
    ];
    jupyterlab = [
      jupyterlab
      notebook
    ];
  };

  pythonImportsCheck = [
    "manim"
  ];

  meta = {
    description = "Animation engine for explanatory math videos";
    homepage = "https://pypi.org/project/manim";
    license = lib.licenses.mit;
    mainProgram = "manim";
  };
}
