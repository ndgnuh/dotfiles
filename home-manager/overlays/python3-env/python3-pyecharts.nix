{
  lib,
  python3,
  fetchPypi,
  ...
}:

python3.pkgs.buildPythonPackage rec {
  pname = "pyecharts";
  version = "2.0.7";
  pyproject = true;

  src = fetchPypi {
    inherit pname version;
    hash = "sha256-6YvHmDNFw4ySWSnhWG3+8sB4iZnrxGYBZYIsGu0F7vY=";
  };

  build-system = [
    python3.pkgs.setuptools
    python3.pkgs.wheel
  ];

  dependencies = with python3.pkgs; [
    jinja2
    prettytable
    simplejson
  ];

  optional-dependencies = with python3.pkgs; {
    images = [
      pil
    ];
    phantomjs = [
      snapshot-phantomjs
    ];
    pyppeteer = [
      snapshot-pyppeteer
    ];
    selenium = [
      snapshot-selenium
    ];
  };

  pythonImportsCheck = [
    "pyecharts"
  ];

  meta = {
    description = "Python options, make charting easier";
    homepage = "https://pypi.org/project/pyecharts/";
    license = lib.licenses.mit;
    maintainers = with lib.maintainers; [ ];
    mainProgram = "pyecharts";
  };
}
