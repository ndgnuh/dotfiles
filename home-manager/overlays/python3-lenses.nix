final: prev:
let
  inherit (final) lib fetchPypi;
in
{
  pythonPackagesExtensions = prev.pythonPackagesExtensions ++ [
    (python-final: python-prev: {
      lenses = python-prev.pkgs.buildPythonPackage rec {
        pname = "lenses";
        version = "1.1.0";

        src = fetchPypi {
          inherit pname version;
          hash = "sha256-yQWDP4ktzvRd5EEaQ/H/MJN3SBV7arUYH8oGO7CVzoE=";
        };

        pythonImportsCheck = [
          "lenses"
        ];

        meta = {
          description = "A lens library for python";
          homepage = "https://pypi.org/project/lenses/";
          license = lib.licenses.gpl3Only;
        };
      };
    })
  ];
}
