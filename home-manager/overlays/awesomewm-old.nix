final: prev:
let
  ibus-env = prev.ibus-with-plugins.override {
    plugins = with prev.ibus-engines; [
      anthy
      bamboo
    ];
  };

  giLibs = with final.pkgs; [
    playerctl
    upower
    ibus-env
    networkmanager
    playerctl
    pango
    gtk3
    lightdm
  ];

  lib = final.lib;
  mkTypelibPath = pkg: "${pkg}/lib/girepository-1.0";
  giTypelibPath = lib.forEach giLibs mkTypelibPath;
in
{
  final.awesomewm-custom = prev.awesome.overrideAttrs (old: {
    buildInputs = old.buildInputs ++ giLibs;
    GI_TYPELIB_PATH = lib.concatStringsSep ":" giTypelibPath;
    gtk3Support = true;
    cmakeFlags = old.cmakeFlags ++ [ "-DGENERATE_MANPAGES=OFF" ];
  });
}
