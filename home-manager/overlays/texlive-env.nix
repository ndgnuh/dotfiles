final: prev:
let
  # Cleandigo custom theme
  # ======================
  beamer-cleandigo = final.stdenvNoCC.mkDerivation {
    pname = "beamer-cleandigo";
    version = "1.0.0";

    src = final.fetchgit {
      url = "https://gitlab.com/ndgnuh/beamer-theme-cleandigo";
      rev = "ae20d10a39cd2a20caeaa197fa8f723dfada7ebe";
      sha256 = "sha256-8RjlCbSN/THJa3qvYRzxhoKGusjyV2bWCkN1EBsDpMU=";
    };

    passthru.tlType = "run";
    patchPhase = ''
      rm Makefile
      mkdir tex/latex -p
      cp *.sty tex/latex
    '';

    buildPhase = ''
      echo "Does nothing"
    '';

    installPhase = ''
      mkdir -p "$out/tex/latex"
      cp tex $out/ -r
    '';
  };
in
{
  texlive-env = final.texlive.combine {
    inherit (final.texlive) scheme-full;
    beamer-cleandigo.pkgs = [ beamer-cleandigo ];
  };
}
