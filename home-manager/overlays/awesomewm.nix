final: prev:
let
  ibus-env = final.ibus-with-plugins.override {
    plugins = with final.ibus-engines; [
      anthy
      bamboo
    ];
  };

  libs = with final; [
    playerctl
    upower
    ibus-env
    networkmanager
    playerctl
    pango
    gtk3
    lightdm
  ];

  lib = final.lib;
  mkLibPath = pkg: "${pkg}/lib/girepository-1.0";
  libPaths = lib.forEach libs mkLibPath;
  libPathString = lib.concatStringsSep ":" libPaths;
in
{
  awesome = prev.awesome.overrideAttrs (old: {
    buildInputs = old.buildInputs ++ libs;
    GI_TYPELIB_PATH = "${old.GI_TYPELIB_PATH}:${libPathString}";
  });
}
