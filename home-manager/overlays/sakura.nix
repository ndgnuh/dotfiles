final: prev: {
  sakura = prev.sakura.overrideAttrs (old: {
    version = "9db243cd2dac2f9fa1952e6835ccbe3959644a5b";
    src = prev.fetchFromGitHub {
      owner = "ndgnuh";
      repo = "sakura";
      rev = "9db243cd2dac2f9fa1952e6835ccbe3959644a5b";
      hash = "sha256-2YZEXtQqz+FcN2OjLXEV3sOtgeIPomU9BlMAFjeuxZw=";
    };
  });
}
