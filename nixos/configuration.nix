# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{
  config,
  lib,
  pkgs,
  ...
}:
let
  nixgl = import <nixgl> { };
  homeManager = import <home-manager> { };

in
# Custom packages
# ===================
# callPkg = pkgs.callPackage;
# cwc = callPkg ./mypkgs/cwcwm.nix { };
{
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    <home-manager/nixos>
    # ./mypkgs/cwcwm-session.nix
  ];

  # Overlays
  nixpkgs.overlays = [
    # Languages
    (import ./overlays/julia.nix)
    # (import ./overlays/python3-lenses.nix)
    (import ./overlays/python3-env)
    (import ./overlays/texlive-env.nix)

    # Desktop misc
    (import ./overlays/awesomewm.nix)
    (import ./overlays/sakura.nix)
    (import ./overlays/distro-grub-theme-nixos.nix)
  ];

  # OpenGL
  hardware.graphics = {
    enable = true;
    enable32Bit = true;
    extraPackages = with pkgs; [
      vpl-gpu-rt
      intel-media-sdk
      intel-gmmlib
      intel-media-driver
      vulkan-tools
    ];
    extraPackages32 = with pkgs.pkgsi686Linux; [
      vulkan-tools
      # vpl-gpu-rt
      intel-gmmlib
      intel-media-driver
    ];
    # extraPackages32 = with pkgs.pkgsi686Linux; [
    #   vpl-gpu-rt
    #   intel-gmmlib
    #   intel-media-driver
    # ];
  };

  # FUSE options
  programs.fuse.userAllowOther = true;

  # NixDL
  programs.nix-ld.enable = true;
  programs.nix-ld.libraries = with pkgs; [
    mesa
    libGL
    libGLU
    glibc
  ];

  # Virtualization, docker and distrobox
  virtualisation.docker.enable = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.grub = {
    enable = true;
    device = "nodev";
    useOSProber = true;
    efiSupport = true;
    theme = "${pkgs.distro-grub-theme-nixos}/grub/themes/distro-theme-nixos";
    splashImage = null;
    # gfxmodeEfi = "1920x1200";
    # gfxmode = "2240x1400";
    gfxmodeEfi = "2240x1400";
    font = "${pkgs.notonoto}/share/fonts/notonoto/NotoSansMono-Regular.ttf";
    fontSize = 24;
  };
  boot.loader.grub.extraEntries = ''
    # Shutdown
    menuentry "Shutdown" {
      halt
    }

    # Reboot
    menuentry "Reboot" {
      reboot
    }
  '';
  boot.kernelParams = [
    "i915.force_probe=7d45"
    "i915.enable_guc=3"
    "sysrq_always_enabled=1"
  ];
  boot.kernelPackages = pkgs.linuxPackages_lqx;
  boot.tmp.useTmpfs = true;

  # networking.hostName = "nixos"; # Define your hostname.
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true; # Easiest to use and most distros use this by default.

  # Set your time zone.
  time.timeZone = "Asia/Ho_Chi_Minh";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.supportedLocales = [ "all" ];
  i18n.inputMethod = {
    enable = true;
    type = "ibus";
    ibus.engines = with pkgs.ibus-engines; [
      anthy
      bamboo
    ];
  };

  # Fonts
  fonts.fontDir.enable = true;
  fonts.packages = with pkgs; [
    noto-fonts-cjk-serif
    noto-fonts-cjk-sans
    notonoto
    noto-fonts-emoji-blob-bin
    inconsolata
    inconsolata-nerdfont
    julia-mono
    liberation_ttf # Core fonts replacement
    paratype-pt-serif
  ];
  fonts.fontconfig = {
    enable = true;
    hinting = {
      enable = true;
      style = "slight";
    };
    subpixel = {
      rgba = "rgb";
      lcdfilter = "default";
    };
    defaultFonts = {
      serif = [ "Noto Serif CJK JP" ];
      sansSerif = [ "Noto Sans CJK JP" ];
      monospace = [
        "Noto Sans Mono"
        "Inconsolata"
        "Inconsolata Nerd Font"
      ];
      emoji = [ "Noto Color Emoji" ];
    };
  };

  # Environment variables
  environment.variables = {
    GTK_IM_MODULE = "ibus";
    QT_IM_MODULE = "ibus";
    XMODIFIERS = "@im=ibus";
    QT_QPA_PLATFORMTHEME = "qt5ct";
    XCURSOR_SIZE = "48";
    # QT_STYLE_OVERRIDE = "kvantum";
  };
  environment.sessionVariables = {
    EDITOR = "nvim";
    BROWSER = "firefox-devedition";
    JULIA_NUM_THREADS = 8;
    JULIA_DEPOT_PATH = "$HOME/.cache/julia:";
    ANDROID_HOME = "$HOME/Application/Android/";
    ANDROID_SDK_ROOT = "$ANDROID_HOME";
    FLUTTER_HOME = "$HOME/Application/flutter/flutter";
    CHROME_EXECUTABLE = "chromium";
    GTK_IM_MODULE = "ibus";
    QT_IM_MODULE = "ibus";
    QT4_IM_MODULE = "ibus";
    XMODIFIERS = "@im=ibus";

    # https://github.com/nix-community/NixOS-WSL/issues/454
    # Replace `hardware.opengl.setLdLibraryPaht = true`
    LD_LIBRARY_PATH = [
      "/run/opengl-driver/lib"
      "/run/opengl-driver-32/lib"
    ];
  };

  # Custom /etc configs
  environment.etc = {
    "gtk-3.0/settings.ini".text = ''
      [Settings]
      gtk-cursor-theme-name=Vimix Cursors
      gtk-icon-theme-name=Vimix
      gtk-theme-name=Yaru
    '';
  };

  console = {
    font = "Lat2-Terminus16";
    # keyMap = "us";
    useXkbConfig = true; # use xkb.options in tty.
  };

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    dpi = 144;
    upscaleDefaultCursor = true;
    videoDrivers = [ "modesetting" ];
    deviceSection = ''
      Option "TearFree"        "false"
      Option "TripleBuffer"    "false"
      Option "SwapbuffersWait" "false"
      Option "DRI" "iris"
    '';
  };
  services.xserver.displayManager = {
    lightdm = {
      enable = true;
      greeters.slick = {
        enable = true;
        cursorTheme.size = 36;
        cursorTheme.name = "Vimix";
        iconTheme.name = "Vimix Cursors";
        theme.name = "Yaru";
        font.package = pkgs.noto-fonts-cjk-sans;
        font.name = "Noto Sans CJK JP 16";
        extraConfig = ''
          draw-grid = true
          show-hostname = true
          show-power = true
          show-clock = true
          show-quit = true
          xft-antialias = true
          xft-dpi = 144
          xft-hintstyle = hintslight
          xft-rgba = rgb
        '';
      };
    };
  };
  programs.hyprland.enable = true;
  services.xserver.windowManager = {
    qtile = {
      enable = true;
      extraPackages =
        p: with pkgs; [
          mako # notification daemon
          libnotify # notification library
          p.pygobject3 # GObject introspection
        ];
    };
    # cwc.enable = true;
    awesome = {
      enable = true;
      luaModules = with pkgs.luaPackages; [
        lgi
        luarocks
      ];
    };
  };

  # XDG specification config
  xdg = {
    portal = {
      enable = true;
      config.common.default = [
        "gtk"
        "lxqt"
      ];
    };
    autostart.enable = true;
    icons.enable = true;
    sounds.enable = true;
    mime.enable = true;
    menus.enable = true;
  };

  # Configure keymap in X11
  services.xserver.xkb.layout = "us";
  # services.xserver.xkb.options = "eurosign:e,caps:escape";

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.drivers = with pkgs; [
    gutenprint # — Drivers for many different printers from many different vendors.
    gutenprintBin # — Additional, binary-only drivers for some printers.
    hplip # — Drivers for HP printers.
    hplipWithPlugin # — Drivers for HP printers, with the proprietary plugin. Use NIXPKGS_ALLOW_UNFREE=1 nix-shell -p hplipWithPlugin --run 'sudo -E hp-setup' to add the printer, regular CUPS UI doesn't seem to work.
    postscript-lexmark # — Postscript drivers for Lexmark
    samsung-unified-linux-driver # — Proprietary Samsung Drivers
    splix # — Drivers for printers supporting SPL (Samsung Printer Language).
    brlaser # — Drivers for some Brother printers
    brgenml1lpr # — Generic drivers for more Brother printers [1]
    brgenml1cupswrapper # — Generic drivers for more Brother printers [1]
    cnijfilter2 # — Drivers for some Canon Pixma devices (Proprietary driver)
    canon-cups-ufr2
    canon-capt
    cups-bjnp

    # Brother drivers
    cups-brother-mfcl2800dw
    cups-brother-mfcl2750dw
    cups-brother-hll3230cdw
    cups-brother-hll2375dw
    cups-brother-hll2350dw
    cups-brother-hll2340dw
    cups-brother-hl3170cdw
    cups-brother-hl3140cw
    cups-brother-hl2260d
    cups-brother-hl1210w
    cups-brother-hl1110
  ];
  services.printing.extraConf = ''
    DefaultPaperSize A4
  '';

  # Discover printers
  services.printing.browsedConf = ''
    BrowseDNSSDSubTypes _cups,_print
    BrowseLocalProtocols all
    BrowseRemoteProtocols all
    CreateIPPPrinterQueues All

    BrowseProtocols all
  '';
  services.avahi = {
    enable = true;
    nssmdns4 = true;
  };

  # Enable sound.
  hardware.pulseaudio.enable = false;
  # OR
  services.pipewire = {
    enable = true;
    pulse.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  services.libinput.enable = true;
  services.libinput.touchpad = {
    horizontalScrolling = true;
    tappingDragLock = false;
    scrollMethod = "twofinger";
    accelSpeed = "0.5";
  };

  # Bluetooth
  hardware.bluetooth.enable = true;
  services.blueman.enable = true;
  hardware.bluetooth.powerOnBoot = false;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.hung = {
    shell = pkgs.zsh;
    isNormalUser = true;
    extraGroups = [
      "libvirtd"
      "vboxusers"
      "wheel" # Enable ‘sudo’ for the user.
      "networkmanager" # Allow controlling the network without root
      "docker" # Using docker without root
      "lp" # Manage printer without root
    ];
  };

  # misc services
  services.tailscale.enable = false; # Disabled by default
  services.upower.enable = true;
  services.udisks2.enable = true;
  services.thinkfan.enable = true;
  services.gvfs.enable = true;
  services.tumbler.enable = true;
  services.cloudflare-warp.enable = true;
  powerManagement.powertop.enable = true;

  # Waydroid
  virtualisation.waydroid.enable = false; # rarely used

  # Virtualbox
  virtualisation.virtualbox = {
    host = {
      enable = true;
    };
    guest = {
      enable = true;
      dragAndDrop = true;
      clipboard = true;
    };
  };
  users.extraGroups.vboxusers.members = [ "hung" ];

  # Libvert
  virtualisation.libvirtd.enable = true;
  programs.virt-manager.enable = true;

  # TLP
  services.tlp = {
    enable = true;
    settings = {
      START_CHARGE_THRESH_BAT0 = 60;
      STOP_CHARGE_THRESH_BAT0 = 80;
    };
  };

  # Programs configuration
  programs = {
    zsh.enable = true;
    thunar = {
      enable = true;
      plugins = with pkgs; [
        xfce.thunar-volman
        xfce.thunar-dropbox-plugin
        xfce.thunar-archive-plugin
        xfce.thunar-media-tags-plugin
      ];
    };
  };

  # enable nix command
  nix.settings.experimental-features = [ "nix-command" ];

  environment.localBinInPath = true;
  environment.shellAliases = {
    l = "ls -alh --color=auto";
    nrs = "sudo nixos-rebuild switch";
    src = "exec $SHELL";
    feh = "feh --scale-down";
    # julia = "nixGL julia";
    pluto-notebook = ''julia -e "using Pluto; Pluto.run()"'';
    octave-gui = "octave --gui";
    pencil = "pencil --no-sandbox";
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    # system tools
    xdotool
    libsForQt5.kdialog
    powershell
    hw-probe
    coreboot-utils
    distro-grub-theme-nixos
    grub2
    os-prober
    cargo
    gparted
    parted
    pciutils
    htop
    gnupg
    git
    zsh
    neovim
    file
    tmux
    feh
    nmap
    yt-dlp
    mediainfo
    ffmpeg-full
    imagemagickBig
    ghostscript_headless
    fzf
    lm_sensors
    psmisc
    inxi
    zcfan
    nixgl.nixGLIntel
    nixgl.nixVulkanIntel
    docker
    distrobox
    nix-index
    mesa-demos
    lsb-release
    hardinfo
    libsForQt5.qt5ct
    libsForQt5.qtstyleplugin-kvantum
    kdePackages.qtstyleplugin-kvantum
    neofetch
    aria2
    ventoy-full
    nix-init

    # Android
    android-tools
    adbfs-rootless
    flutter
    android-studio-tools
    waydroid

    # Intel drivers
    vpl-gpu-rt
    intel-media-sdk
    intel-gmmlib
    intel-media-driver

    # Inxi reccommends
    bluez-tools
    dmidecode
    doas
    system-config-printer
    hddtemp
    usbutils
    mdadm
    busybox
    smartmontools
    tree
    vulkan-tools
    vulkan-loader
    wayland-utils
    wmctrl
    xorg.xdriinfo
    entr
    gnumake
    cmake

    # XDG tools
    xdgmenumaker
    xdg-utils
    xdg-user-dirs
    xdg-desktop-portal-gtk
    xdg-desktop-portal
    lxqt.xdg-desktop-portal-lxqt
    dex

    # Filesystem and Synchronization
    rsync
    ncdu
    stow
    rclone
    syncthing
    wget
    curl
    localsend
    fuse
    fuse3
    fuseiso
    sshfs
    bindfs

    # Productivity
    gsimplecal
    telegram-desktop
    inkscape-with-extensions
    pencil
    obs-studio
    kdenlive
    krita
    gimp
    vlc
    onlyoffice-desktopeditors
    pandoc
    firefox-devedition
    chromium
    keepassxc
    qpdfview
    xreader
    mupdf
    aegisub
    # peazip

    # Tex stuffs
    texlive-env
    lyx
    texstudio
    poppler_utils

    # VPNS
    protonvpn-gui
    protonvpn-cli
    openvpn3
    networkmanager-openvpn
    networkmanagerapplet
    tailscale

    # Themes
    yaru-theme
    materia-theme
    materia-kde-theme
    vimix-icon-theme
    vimix-cursors
    vimPlugins.nvim-web-devicons

    # Dotfile manager
    homeManager.home-manager

    # Music
    mpd
    mpdris2
    mpd-mpris
    cantata
    mpc-cli
    ncmpcpp
    pulseaudio
    playerctl

    # Hardware stuffs
    iucode-tool
    light
    brightnessctl
    microcodeIntel

    # Xorg stuffs
    xorg.xdpyinfo
    xorg.xev
    xorg.xkill
    xorg.xhost
    xorg.xinit
    xorg.xeyes
    xsettingsd
    xclip
    arandr
    copyq
    pavucontrol
    gobject-introspection
    copyq
    libnotify
    xdragon
    redshift

    # Screenshots
    gscreenshot
    kdePackages.spectacle
    slop
    scrot

    # Language servers
    texlab
    nixd
    nix-plugins
    nixfmt-rfc-style
    yaml-language-server
    vue-language-server
    vim-language-server
    rust-analyzer # Rust LSP
    biome
    rustfmt # Rust Formatter
    clippy # Rust Linter
    matlab-language-server
    lua-language-server
    jdt-language-server
    elmPackages.elm-language-server
    dockerfile-language-server-nodejs
    nodePackages.bash-language-server
    vim-language-server
    sql-formatter
    sqls
    vscode-langservers-extracted
    tree-sitter

    # Misc
    tailscale
    efibootmgr
    luaPackages.lgi
    zenity
    kdialog

    # Wine
    bottles
    winetricks
    wineWowPackages.stagingFull
    lutris

    # Archives
    p7zip
    unzip
    zip
    rar
    unrar
    xar

    # Languages and runtimes
    typst
    typstyle
    bun
    octaveFull
    gcc
    geckodriver # For selenium
    selenium-manager
    # selenium-server-standalone
    # jupyter
    python3-env
    flutter
    maxima
    wxmaxima
    julia-custom
    # julia-lts # julia 1.10-bin in nixos-24.11 channel
    julia-build-sysimage
    sqlitebrowser
    gibo # Generate git ignore
    nodejs

    # Terminals
    lxde.lxappearance
    sakura
    dmenu
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Finger print reader
  services.fprintd.enable = true;
  services.fprintd.tod.enable = true;
  services.fprintd.tod.driver = pkgs.libfprint-2-tod1-goodix;
  # libfprint-2-tod1-goodix
  # libfprint-2-tod1-goodix-550a

  # Update firmware
  services.fwupd.enable = true;

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  networking.firewall =
    let
      ports = [
        # The usual..
        80
        443
        8000
        8080
        8888
        1234
        # localsend
        53317
        # Syncthing
        22000
        21027
      ];
      udpPorts = [
        # Open VPN
        1194
        # Cloudflare Warp
        2408
        500
        1701
        4500
      ];
    in
    {
      enable = true;
      allowedTCPPorts = [ ] ++ ports;
      allowedUDPPorts = udpPorts ++ ports;
    };
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Unfree
  nixpkgs.config.allowUnfree = true;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system - see https://nixos.org/manual/nixos/stable/#sec-upgrading for how
  # to actually do that.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "24.11"; # Did you read the comment?
}
