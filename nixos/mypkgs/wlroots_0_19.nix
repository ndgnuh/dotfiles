{
  pkgs ? import <nixpkgs> { },
  lib ? pkgs.lib,
  stdenv ? pkgs.stdenv,
}:
let
  enableXWayland = true;
  xwayland = pkgs.xwayland;

  version = "0.19.0";
  hash = "sha256-BlI3EUoGEHdO6IBh99o/Aadct2dd7Xjc4PG0Sv+flqI=";
  extraNativeBuildInputs = with pkgs; [
    hwdata
    cmake
  ];
  extraBuildInputs = with pkgs; [
    ffmpeg
    libliftoff
    libdisplay-info
    lcms2
    xwayland
  ];
in
stdenv.mkDerivation {
  pname = "wlroots_0_19";
  version = "0.19.0";

  enableXWayland = enableXWayland;

  src = pkgs.fetchFromGitLab {
    domain = "gitlab.freedesktop.org";
    owner = "wlroots";
    repo = "wlroots";
    rev = "c6de47d4158b95a69f678b4131d6a0f8fd9032f0";
    hash = "sha256-dRNCcJkcWpWReoSfdo27ps0YjUGAkL0EhnnrK1goCYc=";
  };

  # $out for the library and $examples for the example programs (in examples):
  outputs = [
    "out"
    "examples"
  ];

  strictDeps = true;
  depsBuildBuild = with pkgs; [ pkg-config ];

  nativeBuildInputs =
    with pkgs;
    [
      meson
      ninja
      pkg-config
      wayland-scanner
      glslang
    ]
    ++ extraNativeBuildInputs;

  buildInputs =
    with pkgs;
    [
      xwayland
      libGL
      libcap
      libinput
      libpng
      libxkbcommon
      mesa
      pixman
      seatd
      vulkan-loader
      wayland
      wayland-protocols
      xorg.libX11
      xorg.xcbutilerrors
      xorg.xcbutilimage
      xorg.xcbutilrenderutil
      xorg.xcbutilwm
      pkgs.xwayland
    ]
    ++ extraBuildInputs;

  postFixup = ''
    # Install ALL example programs to $examples:
    # screencopy dmabuf-capture input-inhibitor layer-shell idle-inhibit idle
    # screenshot output-layout multi-pointer rotation tablet touch pointer
    # simple
              mkdir -p $examples/bin
              cd ./examples
              for binary in $(find . -executable -type f -printf '%P\n' | grep -vE '\.so'); do
                  cp "$binary" "$examples/bin/wlroots-$binary"
                      done
  '';

  # Test via TinyWL (the "minimum viable product" Wayland compositor based on wlroots):
  # passthru.tests = {
  #   tinywl = nixosTests.tinywl;
  #   pkg-config = testers.hasPkgConfigModules {
  #     package = finalAttrs.finalPackage;
  #   };
  # };

  meta = {
    description = "Modular Wayland compositor library";
    longDescription = ''
      Pluggable, composable, unopinionated modules for building a Wayland
      compositor; or about 50,000 lines of code you were going to write anyway.
    '';
    homepage = "https://gitlab.freedesktop.org/wlroots/wlroots";
    changelog = "https://gitlab.freedesktop.org/wlroots/wlroots/-/tags/${version}";
    license = lib.licenses.mit;
    platforms = lib.platforms.linux;
    maintainers = with lib.maintainers; [
      primeos
      synthetica
      rewine
    ];
    pkgConfigModules = [ "wlroots-0.19" ];
  };
}
