{
  pkgs ? import <nixpkgs> { },
  makeWrapper ? pkgs.makeWrapper,
}:
let
  # wlroots019 = (import ./wlroots.nix { }).wlroots_0_19;

  wlroots019 = import ./wlroots_0_19.nix { };

  lua = pkgs.luajit;
  luaEnv = lua.withPackages (ps: [
    ps.lgi
    ps.ldoc
  ]);

in
pkgs.stdenv.mkDerivation rec {
  pname = "cwcwm";
  version = "0.git";

  # src = ./main.zip;
  # unpackPhase = ''unzip $src'';
  src = pkgs.fetchgit {
    url = "https://github.com/ndgnuh/cwcwm.git";
    rev = "345a8dca0b74a8f93cddb32bdd8fee300060dc68";
    sha256 = "sha256-bGKpo46fOmCIIRT/O523Q4QQVPt9grcrCF22802Noq8=";
  };

  # LUA_CPATH and LUA_PATH are used only for *building*, see the --search flags
  # below for how awesome finds the libraries it needs at runtime.
  GI_TYPELIB_PATH = "${pkgs.pango.out}/lib/girepository-1.0";
  LUA_CPATH = "${luaEnv}/lib/lua/${lua.luaversion}/?.so";
  LUA_PATH = "${luaEnv}/share/lua/${lua.luaversion}/?.lua;";

  patchPhase = ''
    sed -i 's/drm_fourcc.h/drm\/drm_fourcc.h/g' src/input/cursor.c
    sed -i 's/drm_fourcc.h/drm\/drm_fourcc.h/g' src/layout/container.c
    sed -ie "s@/usr@$out@" src/meson.build;
    sed -ie "s@/usr@$out@" meson.build;
    cat meson.build
    cat src/meson.build
  '';

  postInstall = ''
    mv "$out/bin/cwc" "$out/bin/.cwc-wrapped"
    makeWrapper "$out/bin/.cwc-wrapped" "$out/bin/cwc" \
          --set GDK_PIXBUF_MODULE_FILE "$GDK_PIXBUF_MODULE_FILE" \
          --add-flags "--library $out/share/cwc/lib" \
          --add-flags "--library $out/share/cwc/plugins" \
          --add-flags "--library ${luaEnv}/lib/lua/${lua.luaversion}" \
          --add-flags "--library ${luaEnv}/share/lua/${lua.luaversion}" \
          --prefix GI_TYPELIB_PATH : "$GI_TYPELIB_PATH"
    echo "${luaEnv}/lib/lua"
  '';

  nativeBuildInputs = with pkgs; [
    unzip
    meson
    cmake
    ninja
    git
    pkg-config
    doxygen
    imagemagick
    makeWrapper
    pkg-config
    xmlto
    docbook_xml_dtd_45
    docbook_xsl
    findXMLCatalogs
    asciidoctor
    gobject-introspection
  ];

  buildInputs = with pkgs; [
    luaEnv
    lua
    wayland
    wayland-protocols
    hyprcursor
    pango
    cairo
    libinput
    xxHash
    xorg.libxcb
    libxkbcommon
    wlroots019
    wayland-scanner
    xwayland
    xorg.xcbutilwm.dev
    boost.dev
    libdrm.dev
    gobject-introspection
  ];

  passthru = {
    inherit lua;
  };
}
