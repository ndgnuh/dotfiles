{
  config,
  lib,
  pkgs,
  ...
}:

with lib;

let

  cfg = config.services.xserver.windowManager.cwc;
  cwc = import ./cwcwm.nix { };
  getLuaPath = lib: dir: "${lib}/${dir}/lua/${cwc.lua.luaversion}";
  makeSearchPath = lib.concatMapStrings (
    path: " --library " + (getLuaPath path "share") + " --library " + (getLuaPath path "lib")
  );
in

{

  ###### interface

  options = {

    services.xserver.windowManager.cwc = {

      enable = mkEnableOption "Awesome window manager WL";

      luaModules = mkOption {
        default = [ ];
        type = types.listOf types.package;
        description = "List of lua packages available for being used in the cwc configuration.";
        example = literalExpression "[ pkgs.luaPackages.vicious ]";
      };

      package = mkPackageOption pkgs "cwc" { };
    };

  };

  ###### implementation

  config = mkIf cfg.enable {

    services.xserver.windowManager.session = singleton {
      name = "cwc";
      start = ''
        ${cwc}/bin/cwc  ${makeSearchPath cfg.luaModules} &
        waitPID=$!
      '';
    };

    environment.systemPackages = [ cwc ];
  };
}
