{
  lib,
  fetchPypi,
  python3,
}:
python3.pkgs.buildPythonApplication rec {
  pname = "lenses";
  version = "1.1.0";
  pyproject = true;

  src = fetchPypi {
    inherit pname version;
    hash = "sha256-yQWDP4ktzvRd5EEaQ/H/MJN3SBV7arUYH8oGO7CVzoE=";
  };

  build-system = [
    python3.pkgs.setuptools
    python3.pkgs.wheel
  ];

  optional-dependencies = with python3.pkgs; {
    docs = [
      sphinx
    ];
    lints = [
      flake8
      mypy
      pyrsistent
      ufmt
    ];
    optional = [
      pyrsistent
    ];
    tests = [
      coverage
      hypothesis
      pyrsistent
      pytest
      pytest-coverage
      pytest-sugar
    ];
  };

  pythonImportsCheck = [
    "lenses"
  ];

  meta = {
    description = "A lens library for python";
    homepage = "https://pypi.org/project/lenses/";
    license = lib.licenses.gpl3Only;
    maintainers = with lib.maintainers; [ ];
    mainProgram = "lenses";
  };
}
