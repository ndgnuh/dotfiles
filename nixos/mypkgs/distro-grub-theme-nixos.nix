{ pkgs }:
pkgs.stdenvNoCC.mkDerivation {
  pname = "distro-grub-theme-nixos";
  version = "3.2";

  src = pkgs.fetchFromGitHub {
    owner = "AdisonCavani";
    repo = "distro-grub-themes";
    rev = "66385f0b0733febdb1db3c0e7a45e6d76902a30e";
    hash = "sha256-6BMNjfc2dM2qQeggtgoYgFUYswqGV+0/QjcNPRT1DUg=";
  };

  nativeBuildInputs = [ pkgs.gnutar ];
  buildInputs = [ ];

  outputs = [ "out" ];

  installPhase = ''
    runHook preInstall

    mkdir -p "$out/grub/themes/distro-theme-nixos"
    tar xvf themes/nixos.tar -C "$out/grub/themes/distro-theme-nixos"

    runHook postInstall
  '';
}
