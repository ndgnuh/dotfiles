nix-channel --add https://github.com/nix-community/home-manager/archive/release-24.11.tar.gz home-manager
nix-channel --add https://github.com/nix-community/nixgl/archive/master.tar.gz nixgl
nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable
nix-channel --update
